PGDMP                         y            serviziosanitario    12.0    12.0 5    K           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            L           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            M           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            N           1262    16498    serviziosanitario    DATABASE     �   CREATE DATABASE serviziosanitario WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Italian_Italy.1252' LC_CTYPE = 'Italian_Italy.1252';
 !   DROP DATABASE serviziosanitario;
                postgres    false                        3079    16499 	   adminpack 	   EXTENSION     A   CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;
    DROP EXTENSION adminpack;
                   false            O           0    0    EXTENSION adminpack    COMMENT     M   COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';
                        false    1            �            1259    16508    Exams    TABLE     !  CREATE TABLE public."Exams" (
    id_patient integer NOT NULL,
    id_doctor integer NOT NULL,
    id_specialized_doctor integer NOT NULL,
    result character varying(255),
    done boolean DEFAULT false,
    id_prescribable integer,
    date_exam date NOT NULL,
    id_ticket integer
);
    DROP TABLE public."Exams";
       public         heap    postgres    false            �            1259    16512    Patient_doctor    TABLE     a   CREATE TABLE public."Patient_doctor" (
    id_patient integer NOT NULL,
    id_doctor integer
);
 $   DROP TABLE public."Patient_doctor";
       public         heap    postgres    false            �            1259    16515 
   Pharmacies    TABLE     �   CREATE TABLE public."Pharmacies" (
    id integer NOT NULL,
    name character varying(25),
    province character varying(25)
);
     DROP TABLE public."Pharmacies";
       public         heap    postgres    false            �            1259    16518    Prescribable_tests    TABLE     �   CREATE TABLE public."Prescribable_tests" (
    id_prescribable integer NOT NULL,
    name character varying(50),
    description character varying(250)
);
 (   DROP TABLE public."Prescribable_tests";
       public         heap    postgres    false            �            1259    16521    Prescriptions    TABLE     �   CREATE TABLE public."Prescriptions" (
    id_prescription integer NOT NULL,
    id_patient integer,
    id_doctor integer,
    description character varying(255),
    date date,
    id_pharmacy integer,
    id_ticket integer,
    date_erog date
);
 #   DROP TABLE public."Prescriptions";
       public         heap    postgres    false            �            1259    16524    Tickets    TABLE     �   CREATE TABLE public."Tickets" (
    id_ticket integer NOT NULL,
    value integer,
    paid boolean,
    "timestamp" date,
    exam boolean
);
    DROP TABLE public."Tickets";
       public         heap    postgres    false            P           0    0    COLUMN "Tickets".exam    COMMENT     u   COMMENT ON COLUMN public."Tickets".exam IS 'true = ticket relativo ad esame
false = ticket relativo a prescrizione';
          public          postgres    false    208            �            1259    16527    Users    TABLE     �  CREATE TABLE public."Users" (
    role character varying(255),
    psw character varying(255),
    name character varying(255),
    surname character varying(255),
    cf character varying(255),
    email character varying(255),
    province character varying(255),
    id_user integer NOT NULL,
    token character varying(255),
    path_imgs text[],
    filename_imgs text[],
    sex character varying(255),
    date_of_birth date,
    salt bytea,
    city character varying(255)
);
    DROP TABLE public."Users";
       public         heap    postgres    false            �            1259    16533    Visits    TABLE     ~   CREATE TABLE public."Visits" (
    id_visit integer NOT NULL,
    id_patient integer,
    id_doctor integer,
    date date
);
    DROP TABLE public."Visits";
       public         heap    postgres    false            A          0    16508    Exams 
   TABLE DATA           �   COPY public."Exams" (id_patient, id_doctor, id_specialized_doctor, result, done, id_prescribable, date_exam, id_ticket) FROM stdin;
    public          postgres    false    203   �>       B          0    16512    Patient_doctor 
   TABLE DATA           A   COPY public."Patient_doctor" (id_patient, id_doctor) FROM stdin;
    public          postgres    false    204   ZB       C          0    16515 
   Pharmacies 
   TABLE DATA           :   COPY public."Pharmacies" (id, name, province) FROM stdin;
    public          postgres    false    205   �B       D          0    16518    Prescribable_tests 
   TABLE DATA           R   COPY public."Prescribable_tests" (id_prescribable, name, description) FROM stdin;
    public          postgres    false    206   GC       E          0    16521    Prescriptions 
   TABLE DATA           �   COPY public."Prescriptions" (id_prescription, id_patient, id_doctor, description, date, id_pharmacy, id_ticket, date_erog) FROM stdin;
    public          postgres    false    207   8J       F          0    16524    Tickets 
   TABLE DATA           N   COPY public."Tickets" (id_ticket, value, paid, "timestamp", exam) FROM stdin;
    public          postgres    false    208   �L       G          0    16527    Users 
   TABLE DATA           �   COPY public."Users" (role, psw, name, surname, cf, email, province, id_user, token, path_imgs, filename_imgs, sex, date_of_birth, salt, city) FROM stdin;
    public          postgres    false    209   EO       H          0    16533    Visits 
   TABLE DATA           I   COPY public."Visits" (id_visit, id_patient, id_doctor, date) FROM stdin;
    public          postgres    false    210   �k       �
           2606    16537    Exams Exams_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public."Exams"
    ADD CONSTRAINT "Exams_pkey" PRIMARY KEY (id_patient, id_doctor, id_specialized_doctor, date_exam);
 >   ALTER TABLE ONLY public."Exams" DROP CONSTRAINT "Exams_pkey";
       public            postgres    false    203    203    203    203            �
           2606    16539 #   Patient_doctor Paziente_medico_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public."Patient_doctor"
    ADD CONSTRAINT "Paziente_medico_pkey" PRIMARY KEY (id_patient);
 Q   ALTER TABLE ONLY public."Patient_doctor" DROP CONSTRAINT "Paziente_medico_pkey";
       public            postgres    false    204            �
           2606    16541    Pharmacies Pharmacy_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public."Pharmacies"
    ADD CONSTRAINT "Pharmacy_pkey" PRIMARY KEY (id);
 F   ALTER TABLE ONLY public."Pharmacies" DROP CONSTRAINT "Pharmacy_pkey";
       public            postgres    false    205            �
           2606    16543     Prescriptions Prescriptions_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public."Prescriptions"
    ADD CONSTRAINT "Prescriptions_pkey" PRIMARY KEY (id_prescription);
 N   ALTER TABLE ONLY public."Prescriptions" DROP CONSTRAINT "Prescriptions_pkey";
       public            postgres    false    207            �
           2606    16545    Users Users_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY (id_user);
 >   ALTER TABLE ONLY public."Users" DROP CONSTRAINT "Users_pkey";
       public            postgres    false    209            �
           2606    16547    Users Utente_email_key 
   CONSTRAINT     V   ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT "Utente_email_key" UNIQUE (email);
 D   ALTER TABLE ONLY public."Users" DROP CONSTRAINT "Utente_email_key";
       public            postgres    false    209            �
           2606    16549    Visits Visits_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public."Visits"
    ADD CONSTRAINT "Visits_pkey" PRIMARY KEY (id_visit);
 @   ALTER TABLE ONLY public."Visits" DROP CONSTRAINT "Visits_pkey";
       public            postgres    false    210            �
           2606    16551 +   Prescribable_tests esami_prescrivibili_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public."Prescribable_tests"
    ADD CONSTRAINT esami_prescrivibili_pkey PRIMARY KEY (id_prescribable);
 W   ALTER TABLE ONLY public."Prescribable_tests" DROP CONSTRAINT esami_prescrivibili_pkey;
       public            postgres    false    206            �
           2606    16553    Tickets pk_ticket 
   CONSTRAINT     X   ALTER TABLE ONLY public."Tickets"
    ADD CONSTRAINT pk_ticket PRIMARY KEY (id_ticket);
 =   ALTER TABLE ONLY public."Tickets" DROP CONSTRAINT pk_ticket;
       public            postgres    false    208            �
           1259    16554 
   fki_doctor    INDEX     D   CREATE INDEX fki_doctor ON public."Visits" USING btree (id_doctor);
    DROP INDEX public.fki_doctor;
       public            postgres    false    210            �
           1259    16555 
   fki_fk_doc    INDEX     L   CREATE INDEX fki_fk_doc ON public."Patient_doctor" USING btree (id_doctor);
    DROP INDEX public.fki_fk_doc;
       public            postgres    false    204            �
           1259    16556    fki_fk_doctor    INDEX     F   CREATE INDEX fki_fk_doctor ON public."Exams" USING btree (id_doctor);
 !   DROP INDEX public.fki_fk_doctor;
       public            postgres    false    203            �
           1259    16557    fki_fk_doctor2    INDEX     O   CREATE INDEX fki_fk_doctor2 ON public."Prescriptions" USING btree (id_doctor);
 "   DROP INDEX public.fki_fk_doctor2;
       public            postgres    false    207            �
           1259    16558 
   fki_fk_pat    INDEX     L   CREATE INDEX fki_fk_pat ON public."Prescriptions" USING btree (id_patient);
    DROP INDEX public.fki_fk_pat;
       public            postgres    false    207            �
           1259    16559    fki_fk_patient    INDEX     H   CREATE INDEX fki_fk_patient ON public."Exams" USING btree (id_patient);
 "   DROP INDEX public.fki_fk_patient;
       public            postgres    false    203            �
           1259    16560    fki_fk_prescribable    INDEX     R   CREATE INDEX fki_fk_prescribable ON public."Exams" USING btree (id_prescribable);
 '   DROP INDEX public.fki_fk_prescribable;
       public            postgres    false    203            �
           1259    16561    fki_fk_specialized    INDEX     W   CREATE INDEX fki_fk_specialized ON public."Exams" USING btree (id_specialized_doctor);
 &   DROP INDEX public.fki_fk_specialized;
       public            postgres    false    203            �
           1259    16562    fki_fk_ticket    INDEX     F   CREATE INDEX fki_fk_ticket ON public."Exams" USING btree (id_ticket);
 !   DROP INDEX public.fki_fk_ticket;
       public            postgres    false    203            �
           1259    16563    fki_patient    INDEX     F   CREATE INDEX fki_patient ON public."Visits" USING btree (id_patient);
    DROP INDEX public.fki_patient;
       public            postgres    false    210            �
           2606    16564    Exams fk_doctor    FK CONSTRAINT     y   ALTER TABLE ONLY public."Exams"
    ADD CONSTRAINT fk_doctor FOREIGN KEY (id_doctor) REFERENCES public."Users"(id_user);
 ;   ALTER TABLE ONLY public."Exams" DROP CONSTRAINT fk_doctor;
       public          postgres    false    209    203    2737            �
           2606    16569    Patient_doctor fk_doctor    FK CONSTRAINT     �   ALTER TABLE ONLY public."Patient_doctor"
    ADD CONSTRAINT fk_doctor FOREIGN KEY (id_doctor) REFERENCES public."Users"(id_user);
 D   ALTER TABLE ONLY public."Patient_doctor" DROP CONSTRAINT fk_doctor;
       public          postgres    false    209    204    2737            �
           2606    16574    Prescriptions fk_doctor    FK CONSTRAINT     �   ALTER TABLE ONLY public."Prescriptions"
    ADD CONSTRAINT fk_doctor FOREIGN KEY (id_doctor) REFERENCES public."Users"(id_user);
 C   ALTER TABLE ONLY public."Prescriptions" DROP CONSTRAINT fk_doctor;
       public          postgres    false    207    2737    209            �
           2606    16579    Visits fk_doctor    FK CONSTRAINT     �   ALTER TABLE ONLY public."Visits"
    ADD CONSTRAINT fk_doctor FOREIGN KEY (id_doctor) REFERENCES public."Users"(id_user) NOT VALID;
 <   ALTER TABLE ONLY public."Visits" DROP CONSTRAINT fk_doctor;
       public          postgres    false    2737    209    210            �
           2606    16584    Exams fk_patient    FK CONSTRAINT     {   ALTER TABLE ONLY public."Exams"
    ADD CONSTRAINT fk_patient FOREIGN KEY (id_patient) REFERENCES public."Users"(id_user);
 <   ALTER TABLE ONLY public."Exams" DROP CONSTRAINT fk_patient;
       public          postgres    false    2737    209    203            �
           2606    16589    Patient_doctor fk_patient    FK CONSTRAINT     �   ALTER TABLE ONLY public."Patient_doctor"
    ADD CONSTRAINT fk_patient FOREIGN KEY (id_patient) REFERENCES public."Users"(id_user);
 E   ALTER TABLE ONLY public."Patient_doctor" DROP CONSTRAINT fk_patient;
       public          postgres    false    209    2737    204            �
           2606    16594    Prescriptions fk_patient    FK CONSTRAINT     �   ALTER TABLE ONLY public."Prescriptions"
    ADD CONSTRAINT fk_patient FOREIGN KEY (id_patient) REFERENCES public."Users"(id_user);
 D   ALTER TABLE ONLY public."Prescriptions" DROP CONSTRAINT fk_patient;
       public          postgres    false    209    207    2737            �
           2606    16599    Visits fk_patient    FK CONSTRAINT     �   ALTER TABLE ONLY public."Visits"
    ADD CONSTRAINT fk_patient FOREIGN KEY (id_patient) REFERENCES public."Users"(id_user) NOT VALID;
 =   ALTER TABLE ONLY public."Visits" DROP CONSTRAINT fk_patient;
       public          postgres    false    209    2737    210            �
           2606    16604    Exams fk_prescribable    FK CONSTRAINT     �   ALTER TABLE ONLY public."Exams"
    ADD CONSTRAINT fk_prescribable FOREIGN KEY (id_prescribable) REFERENCES public."Prescribable_tests"(id_prescribable);
 A   ALTER TABLE ONLY public."Exams" DROP CONSTRAINT fk_prescribable;
       public          postgres    false    206    2729    203            �
           2606    16609    Exams fk_specialized    FK CONSTRAINT     �   ALTER TABLE ONLY public."Exams"
    ADD CONSTRAINT fk_specialized FOREIGN KEY (id_specialized_doctor) REFERENCES public."Users"(id_user);
 @   ALTER TABLE ONLY public."Exams" DROP CONSTRAINT fk_specialized;
       public          postgres    false    2737    203    209            �
           2606    16614    Exams fk_ticket    FK CONSTRAINT     }   ALTER TABLE ONLY public."Exams"
    ADD CONSTRAINT fk_ticket FOREIGN KEY (id_ticket) REFERENCES public."Tickets"(id_ticket);
 ;   ALTER TABLE ONLY public."Exams" DROP CONSTRAINT fk_ticket;
       public          postgres    false    2735    203    208            A   {  x�mVK��6\���F	�����g�x�<���K��>�"Mғ�0\��TW�6F8H���� #��<��(�I���
��P`f{�:ND�ғ�"��`� M`��I ��iEʒ"3	���tbYKk����D[�4�,��Εr#�ԙZd+M��&��b��dBT�{T����A�>q�gz� 2��oW��S��Yz�4��� ��%ִ���{�T�(���2�'m$�9�!�6���h��iT�Z�N�"?��9}AO���蛘l�Hv�� m@f4m�Ѻ_G�;��Y"�;|���qD����C��T���Hf�P84a�`V&��"8fH����,��(�vR������e,�X=T'4A���jK�Gr�)5���� �e������h,�Kd&R1���?���0X[��]v�X�ɔ��J�MOj�
Jm�x��K�J��<^p�I�>p�+	�x{mK���A4ϘoX��]j<�x�ʹ�6��竭؂/?�FO&�\<���<�bw�����%r�A[����f�J��B���(`�iK/��^_)#�TC05T��r�<�����,~�l�����m�W5)����zY����Ǉ��F�(�V���?���~[�����W�1_��}]�i�m�����-��{WpLH-�v:}[7���������j����i��I�x��}_%�T=��וO��8r6�a�4��4�����m����o���uV����}��﷿�}S��z���Ɱ,,u�o��1�˯���x��r���H�^�n͙��z��3l�^��	�G_^?�;7���5�/Ͼ9�m�ڳ3I
-�h]~�繢��]C����X�����$n����9T=��(I�˻���à��|�IJ�J���      B   �   x��� C�d��|�]�����Ɖ���V�o���l�
b��p�@i.6��J�^a�\��L`��O�EP1W�U"�m�:4�]�KX�`��G����*�V�R;�@���w����5n�k��<և�Mu'��1�-�&�      C   =   x�3�tK,�ML�LTN�S�ON��)J�+��2B�%�(�����s�e&��U%r��qqq e�!      D   �  x��XK��6]w���I�N'c�v��a�8�A�TS%��I���5��r�$'ɫ"��q� 3����zU���7�g�M"�e޽��E��&{2[�,�ێ:�$�7��ޱ�i�t昂q�#�����g�ܫ"��� ֙�"<E(������]��ޑoTf�����*b�t�9R�?ꁢ!"�!�v�Y��V�����谳�~���c+��X0Q�Oؽ������<C�36��dz�{2����Z*Ac�l�M�ENʲ'׻�{�4{q3�;���q�(٠��V{��@Y�0�GU�q���]��H�b4:ڽ#�Ew6�V�F*�:A=�G2�t�]GX�
�q��o8�������O�ҕ9�L1��@G���F��'x\��H���4�2�!D��v�M ��A�Ưͪ9]ȌXHt�[�FA�؅�^��u#��a��M1�{8�c�LN��O��b," T���#M��3VE��P@o^���Qe%q���������.^�w�,��ǋ/-#�� ψ�����#��R8�8�m>���\L�F�ii������(LG�m�*��#*c���	�f�{rU��M/�X�������tγ}��rIm�!(�"�|�R�� �#&-��p�$T�|�wj����4���P�ߤ1�ɝՎ�d�yRo��-�B��C߁�a�H/u�K�x�:����y*%�BB0,�/l҇X�\*�y��
�����u����n���ia�Ո���� &B"���{˾�mcO
�\2|����p�bF�U�M�W�����"Dl�����2M5ͥ	4,���W��kI#�xX>��>�!$zr]��\ b�����m�Շ�o>~��ϱ�Iy��(mN�2�i�٢"���㉀Δ�)(T ��U�0dD��k	�l��3H�|��&��Y����-�� 96���!'A�QgȆ����"^�j[���莩XpK�?e-�S$o��������0�WdV~��y�Gh�J�]	#�;�}�м<Tv�5ja_)^����W]�/��s9�lR���'t��Ͻ�)��\�����a��3JT�@����~PZ���������kIX ���,�W[����xts�hi	
d�'eJZg�e�� ��\瀟��SP�1�zt�F���}�˳�B������I�@̒]a�j}ؿ\�0�c}�b�8�쭝{�m�GB�v�����qH.���Lk6����̚-9�Y��jr׮�բz���0�jG>>�t��!o��0=�Y<��!<�Ú�b~�1yQw�T�)=R��2�5�L��%M� 9,��5Kۭ�b���5r]fia��s؞u�p�e��0D#�s�����.�o��EP�K��B��V��6{rc�$���#�xM�%n0�<@�vY�Ү,�	)+�g��| 8<�ظL9�$�Z�:�U	�I�p,r�l�=��0h�Z�N�D������8V�]��s��Y�:��D!�m�A�l�A�X��4�
�9�>�X=&_p���M2dܾ\���q��Y����V�CE+ތ���z��3���,T�SwAF!��2��@�iSh��p���:�d�v
6W~�ؿ.�7{_���b�L�B�FFT����m �s�E ��g|���&.gx����^�1?h��KS?PCt���|�:�%]^'����8"i���A>$Y>�1��U3��ߎ�����      E   >  x�u�ώ�0����h=�1p�����!�F���O}��Md�+������X������1���EPe�@��)@�S�c>S��Jb�,�ǘ+K%�PT��n�b�9J��D��" �cD�%�0�� [=`)5�Է����0PԮpTe�4�%Eq׿F��S�TY,e��Z�r&8H���p:u��@��A'� �ô7�@7�K���OIߵp����L)8������G�D�ԂS3�|L�����:� Ҵr�ڮ*�c��Ӛz�/:�RR�^ �������rgH�J��Q32#'��i,�t��*��&�i��v����V��O�8�!E�Vj�ͫ�Q��u|����4"������ Z�
���z����o�yф���
T��)_�^�s�.!���H�bRE����y�����9��cT�f���*������9�e�=�]��m\&����KPa����ވ��7�.�2+ƺc�j[D2�v���O:�ҟv!����á6QSlۊ�t6ӛ�)J�v{��Ŋ�38K{�~�ct�o'ߺ��hW���q~d~Tda�(�C�9�~=}c����_      F   �  x�m��q�0��P/�`��D*����	E�7�?���z� #����k��͛I�m%K\JG�ܕ*Z:��4�ϵV�t��}X�[���r���n���A9����!?o�����H�P���(,���2������N�ë�gR�T�8��i��#/���@�y�ּ̹־��W���@�zRH����b�'C��J�A���T6h#Yƛ��D�} ���n�KF�\ɻ]wG�>�\X�t������\�9#���7���-��ұ)Ev�����)B'��,���<��NW����/�S��'�:!��Ӧ�k��St�/^�Gda[rۦ5�Ǯ��w�o�M��J�,ܖ��g���ȍ��L�J��|��]��|,��f$_�cE?��8�T�T�DB*�4�"3J@3��a��*�D/+���N9�$b�����td��y�i��6���k��wC��R�aIK�w�t$ݱ=H��:6�a+��� ܛ���R��4��U�w0��3m��A�W��et�U1��"o]�KѺ8��/����zƽ�M���&���X��B��4U] +��*��!ة�Q49�nʑ��|4JnZ_��(�D���������/z�Q�1뎊3�2���o��-3����d�N���ݛ�A����S+�^N���=�V��ׯ���ۻ�      G      x��\�rIr}����}�1�~�G�%�f ����86�nxAB��Y9�_�/�1��A��~1f$�D��:���df�g������˲�j�&�j�����*I�b�մb���Tj�B�`	):�-���Y���n�Z���E7�]}�N��;;�>������0���7�X���Mw��Xu�w?M����/���������W���w�͇�ŗ��[����j����튿�\���ts�{Y��o?����巿��q���x����qW�_�}G��{Z/�7י_��n���5�����↮k��꯷�����|���?~�����{��h�)�(����/>g�T�'�����M�jcK�����+ݮ����F�����$)T�-��Rs���KB�梈���0�����k�t�ė� �����|ze��r�yWx��������~$����M���o�z�c�D�:�$C�%�@��39��.5��j�
T�m�)���V��Cɾ6�t *�Xl͸r6!�Tb�F���w��bwY��z�^�.������4�s+���Q׆9'��s�!�q���W½4�W�����]h~�j�%�7��Vzp�NQ��T�k�N6*�n����W�D_�A#�PP��j���i�ll)�F��P��>Ur&���'�𻻷K�"�\7[�0F��t�i��wV10�u�q�ɺ��%�	�ø� ���`8LJ9c�SR*�hJPA݌�.��9��������	]_/��@J�¾$��m��6�KJ Hh����P��-Y�V��7*ф��\oo��%�+S�+¼E\@��K?��r{��f�:&p�Ap�8J�d�݄�&|)���"P{U\�kpZ!'� �j}��l��q%5]X��9'�@����1�$���� �X���������w�͉q�O��٧�f�1"��=���}��cBDDD?7���H�F�AF(� /��I`Y��Y���j�Rw���/�咞 ��yGJ㝎RF�U̔H��}	��D�/�c����{W�B�	�	�8h��|v�i4��L������`���1��9�vdc���'s����P�ފ��,V����A�C#�$���i�Q9�f3���D2p��:D�"KR��c
e	��m7���z�4u�.���X�����~����	d���1q9L�AL�sL ��o���K�
���H�Q��B�H1j�XR,�Y��$���',>����ɹ��M�1VX[o5�'-L&i-�P�?�����N�N'�B�(�`L�<!��1H����P�'(��ް��sU���X--��^�$t��`єMw�Ϋo&�,���jJ\���#�P�"�e[�6� U]4� !O`9>1��M	�ƞu-r^t������v���)cHF��k�c1�a��C�p4�������\�wR�����z�,$��"	h.�O/AC���B�nӐ�Y���f�-$�EpL�_��X�k7_,�Ի{v�����������n�N�'����x�����H��=o/V.�^AI��ƈ�,�x���m�J��}]o���F�&2؉E����m�Vhe��, M�B��[������~\c�kw�{����g�+�ߚ���ح������]���>���^�r{��Q�&o(+`�`��@ӛ�`}�B����f'����9ķ	ıTAY���|ҖEC��$�q%�U$~�u�[��ҥ�b^�/z7��_��y9��㜓���'W!�����~-��v����?\�s������߆�<�����c���n�У=�7�̲�T�[���9�h!�TӾ�J��.��%�������W��Ң)��
D��a��V��y5�,��r��������F��s�������qr�3�%1�������3�aT[�����T�3JV	�!��)Y��9K:� &�
T/䱉��TIX�(j�YD�bx�
�B���F�\��������nՍf���c/�`k�wK=���;:�����0&v�V���%z[���8 �I&H`�PƄX�ki/X��l�lB�,�E��)kW��*r<�§%؎�6D8�Ҵ�+�-�y�v���;��W���J|�v�pR�	���11�
��#=�2�=&FF�,,�,���V�eR�ib��uq}˱g>��k�� ��ae�HZ�jg��JSU&PRD���.s�\)�c~"�ö?y����v�%���KHP�Ͷd�A[���g2�-����!D^׬pk� �S�6��Bb��Ű̷��b(N8�'�۽����zF[F�ﭘ�ޞ���a�rLJE��=�:Vp�E�['�\t����2��P��WĵXa�{(>��2�edn���f�Zv5i� D�f��L")��E$�1F!��w��\��%�Ȗ���b��LyFe�]of���{�9���v�JH�#!�e`X��j	"޵*<�~��$��T����B�{��c�����-U(,mN0t��J���4����x�_���j�H٦���� �	��v�O�.��j}e�������s��TOQ� �y�Ӛ�]��g�\��4�,o|�������������� ��ޥh�pX}�
"�H�A�+�����f9�b�]\�� &��k]��h�����!?<��q
�����>�#�I;�y�M�8Yk�q�GHYg���8'��Uۖ^c�3�bE$�H�VJ��%	Ę�r������^dq6�p*CI����ӯ��[���t��ɑӎ0�ˬ�Pa߮������,'�dT�~*$X�"�K�ʨ�Z�(�ߛ6�Yn�[P�	���.w'� �\'VGD@UA�ĬVw��k���|�I������Q��O��}��@��g��J)�`vB�d'��L��3F����V����4�<(8DmJ�h]i�NJ�R��*B�H��b�\D���vMUV�r�7*.�ґ�X��ޭ����ww}^b6����;��y�x�d���1h���,8Ꝃ�쳫ko"��pt6�(�
\����J��O�Py�����0��>�٬`'��dY#�C++mZEIx4������n��l:~�͕�C�e�\�����c�O�`5K����C��wRU�2��uʺ�)+-s����K-D���yk�$�!�)���d�+�Dp��f\�H	�)*o�Z���wgS�'V��--/�)[x�I�D�<w]V��UD��BpV6�G��Zh��m�qO]� �����)X�SY;�% R�.�rk���C|��+d��/H7Z��S7�_]}:���p\��� �x�c�%S;��4lK�`U�!���E�Z�!݌���m_E(!��aa�Y���%�eR#�V�}�Ċq�6\�9T����9Cw��-����^���(N�2@wÄ��a�>9&�rP���(�:]o"�fS���2��`��K!��������|E[B��HY/�T-��l�ʠL�ra���$b�
�aA�h�ݏw���������o�=7j(��0xr���{L��Z;q��@�%��8/��M�X���dKIox�"�W�-�Hd4^��Ep鵖h]�!�؈�R�V5��(�7��
h�Ŀ���q}f�\����0|r��{ʿ�d2���4.K��Uł`�"5P#|E�Z'S���"a�ms�3(.A�@�+�c���\��΁RA�(����_�䄘�S��ec6���c�O����u���x�ru��Va��s0v���S�7�� ���1�!��P׉�#�4�A�c���
�� �`P���Jp�<"{P��F
E��R6�����c��;_p~q�Z��.������\��3<>u�g��mg�����!�ퟡ�q�
��%D��������������׵��mB�u�+4O0bv+�Jي���e���-̝|U�5�Z��Qjl*��N��-A�ZJٙ��S|MA�t�k\��a��l$����e�Џ>a�nw�*Y�ch�Ű�QZ����0��� �w�Ǝ3�%|��4h�L�[�K)�Fh_���B@<�&�5�=D5Bvo�״s'o�����Ǯbt�}lS�����ԃ�< j  �_r��qӭJ0�fjF��@�5�]LO4�KX�a�Ɲ��NPn!R�d5p��d�<�'MBc�E9@��B^��v�f��հ����M�-(����2������*.:gf8{�r)���4B�L��V"B��J��kl$a�5P9��D�Ҷ:'^�����
��j'��лa����a���Ⅼ�"d�|..�[
	�+�0X���
��Zj$^���X୉+���`�[	��pኋy��o2���O?tgt��l>��̄{+�p_׏3NR?cO�Q<G	��qs��S-�R��Y)��vZa����'N��Z���Z܁La�=|��D���H �жD�]C�
J�ƅ�n�F���-�����m8�}v�`���k:��}��}A���GR����؅6���Q��BC�n�z�Z3b#I���
�j9�j�"\�i�#c�I�Tq��6	��M�a[�Xu��u�n�~��L���T��۲��a|O��N���I� $`��Eg���ʮ�LK�����D8c�-'�M|�E'SQd��
�䙤JH%#+�׌�U�A��(���z��P��̯�&�/(5S�!#�=���u)4D���P��!�f�q�^KJ��7+0R"�ͰNZ�pX1v�����؍��G�m>i�q�ՙ�s}�떛�	%)� �<���J���;�׍��V�����	L���M+P�
�<lܩV�v������d4��r�t�6�2Ԗ��`��Z`i��d԰� ��j��
��ѐ�f��Yd�
�W9�=C}-���뽓���h��M��j��~�H���X��C	#�d���*{��S�0��q/��P�(p	U��9�$� �bK�yѝMG�$���r1@���=���d�����N=/8��CoHT�r	��Z�>�,8Z,���^�E��[�"��*���uc��8�<�������@���K�$۝" 2X���}6;��O��84Er���?��;vsL�Wy0��4��{�&����+�>}j�ET��Z!!ԘT}]�K�x_u�`�E��9�v?B�v�OI��Z��}Y{s��Ak}���-���uMf�`2�h���^<gO�?&k9X�S;�oϑ~`b�e�xr���,M˲"��K��"�D�1hq]��
-Q�!c�����[��O
&iU��I�ܯ��ǡƧ��'������ ˽�:�J�~�`�v�ya8�w���[�F��
��Mp_��>�}}����Ԟ��@s%�@Q��h gZ�S�r�w��|�m����b�	��F��ͦ�ͬ�x�_wnkGe �*���D� ���ui`îJZ�o#Q'����~��T��"�&%
�[�"^A�A�|��I�>�V�	�U6���(�}�k�.�_f����ѝ�0�uca���S�;&�ч��v����+}��̭��'�r3JEATEc�ު^d�T*!��f�@��!!�E���Mۀ��W�ɵ �� ^&���7 \d���Pf!9������r�Y��u��'�͌=y�c2�����9$����Kz���XZlkk�CD%���{)m!e�2Ց�G��[��5�j�,�m�H
�͠�:hciaJ\����m-�o�n�Ľmggg�W*n�=_� 73NZ?c�>�6�]<���4�Ƨ�Ή >���lxPZSu��(![���u��,1YN|��&Ԣ���Dc����"�>��i��b�/��t}_o�򉔴��F����\���:�L�C)���c�_\;zQOھ5��}Q��J(E���&��n�O��̱���|U��E�:B ⯰�Tkr,�UQrh�R&�`�%��5k%�������b�"Fq��h�}�%7Om����#�)���%�&	���>(�M�dR�8V^]������6�'<� �׺9j"@V��j&ʐɶ(a�88hH !1�JJ�t������.W���bt1�z<t�O�ڗ͔}R���Vpl��;�0�O�-�^�dܮN ���!�FRFy~�ў���Ĵ�G������Z=��+�t�\6)�J�R��0\�A�B؀�{�Ğ�[z:����Tّ�滮m����ԁ|�a��_�	����+Y*��!+���3��_�+�K�(Ho@R
�ioq)Qֵ$��,�ґ�la� 4� ��=uo׋T�X�}~��˫��J�3�yRH�L�k�	���G ��@OaR��C��{��ù�f]���N���V���^�[ �aTU=��#��������	��T�\���x�2?���"f ɞ����l�8�r�[��s��n��m���_f?���e��n~p��_�������g��!{9��m��m �JBN�?���m��)s�ȧ�E�L0�dvl0�"���������JD��;1@��B7�Z����4�d��V�r_��w��xX���T��qn�����HDXU`Y ̒j�nY�1�ѩg�@܂�P�
��
���%���_�#RB�@$v���:�G�� �@�_т)0r�� �KR�Gd�V���P�zy� �����[6g������~v�V��e@NA�����pB�pw�e�t�BeWb�z�@:�u��(����S�G������O���E�R�����c�.�i���YX��ii�'��9�>6�2�79h�L�gN�	)K�	/k����;�Y#���9_8��L�����xj���i�iw�?Y���{9�θ�=�&�AM���_}�'�oNĽ�����w�
n[�~�*���d[����ZG��h���|���
�hZµ���Q ߍ�?�y���[���x|6�a&�й�&��a����I?c�B%`���$7)��:yOnO�(�B���r�.��w|Т9~�N�1���T��_,�<Kb�]��ݢ,�={j�"lhA~�ۏ�|��c�?�}��k����_���<;�"������A�o�-��!&S�Pߜ�M�;V��x�ua$Kx�WklJB�9��A�d�Z��h��B�����#�֐%�Nnw+B���u�ɨ�`yX�_~� ޛO��}�����M�v�eԃ��7,��C�z^��|���Y����?������O������db�Փ�^ �N�(�XƖ�P�*��R�<�O'�|��� ��e      H   P   x�e���0�7ޅ
Ӥ�]����Gh%>>�Px�7��ʀ��j�&jD��W��c���'Q��rW�R��%+�|�9 ,�m�     