/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Lab 10 - Shopping List Implementation
 * UniTN
 */
package com.servizio.sanitario.filters;

import com.servizio.sanitario.persistence.dao.ExamDAO;
import com.servizio.sanitario.persistence.dao.PatientDoctorDAO;
import com.servizio.sanitario.persistence.dao.PharmacyDAO;
import com.servizio.sanitario.persistence.dao.PrescribableDAO;
import com.servizio.sanitario.persistence.dao.PrescriptionDAO;
import com.servizio.sanitario.persistence.dao.VisitDAO;
import com.servizio.sanitario.persistence.dao.TicketDAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import com.servizio.sanitario.persistence.dao.UserDAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Filter that check if the user is authenticated (and authorized).
 *
 * @author Stefano Chirico &lt;stefano dot chirico at unitn dot it&gt;
 * @since 2019.05.04
 */
public class UsersFilter implements Filter {

    private static final boolean DEBUG = true;

    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;

    public UsersFilter() {
    }

    private void doBeforeProcessing(ServletRequest request, ServletResponse response) throws IOException, ServletException, DAOException, SQLException {

        if (request instanceof HttpServletRequest) {
            ServletContext servletContext = ((HttpServletRequest) request).getServletContext();
            HttpSession session = ((HttpServletRequest) request).getSession(true);
            String contextPath = servletContext.getContextPath();
            if (contextPath.endsWith("/")) {
                contextPath = contextPath.substring(0, contextPath.length() - 1);
            }
            request.setAttribute("contextPath", contextPath);

            DAOFactory daoFactory = (DAOFactory) request.getServletContext().getAttribute("daoFactory");
            if (daoFactory == null) {
                throw new RuntimeException(new ServletException("Impossible to get dao factory for user storage system"));
            }
            String token = null;
            try {
                UserDAO userDao = daoFactory.getDAO(UserDAO.class);
                ExamDAO examDao = daoFactory.getDAO(ExamDAO.class);
                PrescriptionDAO prescriptionDao = daoFactory.getDAO(PrescriptionDAO.class);
                PrescribableDAO prescribableDao = daoFactory.getDAO(PrescribableDAO.class);
                PatientDoctorDAO PatientDoctorDao = daoFactory.getDAO(PatientDoctorDAO.class);
                TicketDAO ticketDao = daoFactory.getDAO(TicketDAO.class);
                PharmacyDAO pharmacyDao = daoFactory.getDAO(PharmacyDAO.class);
                VisitDAO visitDao = daoFactory.getDAO(VisitDAO.class);

                Cookie[] cookies = ((HttpServletRequest) request).getCookies();
                
                if (cookies != null) {
                    for (Cookie cookie : cookies) {
                        if (cookie.getName().equals("token")) {
                            token = cookie.getValue().toString();
                        }
                    }
                }

                if (userDao != null) {
                    ((HttpServletRequest) request).getSession().setAttribute("userDao", userDao);
                }
                if (examDao != null) {
                    ((HttpServletRequest) request).getSession().setAttribute("examDao", examDao);
                }
                if (prescriptionDao != null) {
                    ((HttpServletRequest) request).getSession().setAttribute("prescriptionDao", prescriptionDao);
                }
                if (prescribableDao != null) {
                    ((HttpServletRequest) request).getSession().setAttribute("prescribableDao", prescribableDao);
                }
                if (PatientDoctorDao != null) {
                    ((HttpServletRequest) request).getSession().setAttribute("PatientDoctorDao", PatientDoctorDao);
                }
                if (ticketDao != null) {
                    ((HttpServletRequest) request).getSession().setAttribute("ticketDao", ticketDao);
                }
                if (pharmacyDao != null) {
                    ((HttpServletRequest) request).getSession().setAttribute("pharmacyDao", pharmacyDao);
                }
                if (visitDao != null) {
                    ((HttpServletRequest) request).getSession().setAttribute("visitDao", visitDao);
                }
                if (token != null) {
                    session.setAttribute("user", userDao.getByToken(token));
                }
            } catch (DAOFactoryException ex) {
                throw new RuntimeException(new ServletException("Impossible to get dao factory for user storage system", ex));
            }
            
        }
    }

    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {

        //Nothing to post-process
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (DEBUG) {
            log("UsersFilter:doFilter()");
        }

        try {
            doBeforeProcessing(request, response);
        } catch (DAOException ex) {
            Logger.getLogger(UsersFilter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UsersFilter.class.getName()).log(Level.SEVERE, null, ex);
        }

        Throwable problem = null;
        try {
            chain.doFilter(request, response);
        } catch (IOException | ServletException | RuntimeException ex) {
            problem = ex;
            log(ex.getMessage());
//            ex.printStackTrace();
        }

        doAfterProcessing(request, response);

        // If there was a problem, we want to rethrow it if it is
        // a known type, otherwise log it.
        if (problem != null) {
            if (problem instanceof ServletException) {
                throw (ServletException) problem;
            }
            if (problem instanceof IOException) {
                throw (IOException) problem;
            }
            ((HttpServletResponse) response).sendError(500, problem.getMessage());
        }
    }

    /**
     * Return the filter configuration object for this filter.
     *
     * @return
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    @Override
    public void destroy() {
    }

    /**
     * Init method for this filter
     *
     * @param filterConfig
     */
    @Override
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (DEBUG) {
                log("ShoppingListsFilter:Initializing filter");
            }
        }
    }

    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }

    public void log(String msg, Throwable t) {
        filterConfig.getServletContext().log(msg, t);
    }
}
