/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.dao;

import com.servizio.sanitario.persistence.entities.Pharmacy;
import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.util.List;

/**
 *
 * @author asus
 */
public interface PharmacyDAO extends DAO<Pharmacy, Integer> {
    public List<Pharmacy> getAll() throws DAOException;
    public List<Pharmacy> getByProvince(String province) throws DAOException;
}
