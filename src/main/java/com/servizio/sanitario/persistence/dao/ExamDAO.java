/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.dao;

import com.servizio.sanitario.persistence.entities.Exam;
import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import com.servizio.sanitario.persistence.entities.User;
import java.sql.Date;
import java.util.List;

/**
 * All concrete DAOs must implement this interface to handle the persistence 
 * system that interact with {@link User users}.
 * 
 * @author Stefano Chirico &lt;stefano dot chirico at unitn dot it&gt;
 * @since 2019.04.14
 */
public interface ExamDAO extends DAO<Exam, Integer> {
    public List<Exam> getAll() throws DAOException;
    public List<Exam> getAllByPrimaryKey(Integer primaryKey) throws DAOException;
    public Exam getByTicketId(int id) throws DAOException;
    public Exam getLastExam(int id) throws DAOException;
    public void createnewExam(int id_patient,int id_doctor, int id_specialized_doctor, int id_prescribable, Date date_exam) throws DAOException;    
    public void addResult(String result, int id_patient, int id_doctor, int id_specialized_doctor, Date date_exam) throws DAOException;
}

