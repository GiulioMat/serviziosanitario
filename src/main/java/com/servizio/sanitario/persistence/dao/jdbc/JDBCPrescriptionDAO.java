/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.dao.jdbc;

import com.servizio.sanitario.persistence.dao.PrescriptionDAO;
import com.servizio.sanitario.persistence.entities.Prescription;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The JDBC implementation of the {@link UserDAO} interface.
 *
 * @author Stefano Chirico &lt;stefano dot chirico at unitn dot it&gt;
 * @since 2019.04.14
 */
public class JDBCPrescriptionDAO extends JDBCDAO<Prescription, Integer> implements PrescriptionDAO {

    /**
     * The default constructor of the class.
     *
     * @param con the connection to the persistence system.
     *
     * @author Stefano Chirico
     * @since 1.0.0.190414
     */
    public JDBCPrescriptionDAO(Connection con) {
        super(con);
    }

    /**
     * Returns the number of {@link User users} stored on the persistence system
     * of the application.
     *
     * @return the number of records present into the storage system.
     * @throws DAOException if an error occurred during the information
     * retrieving.
     *
     * @author Stefano Chirico
     * @since 1.0.0.190414
     */
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM \"public\".\"Prescriptions\"");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count users", ex);
        }

        return 0L;
    }

    /**
     * Returns the {@link User user} with the primary key equals to the one
     * passed as parameter.
     *
     * @param primaryKey the {@code id} of the {@code user} to get.
     * @return the {@code user} with the id equals to the one passed as
     * parameter or {@code null} if no entities with that id are present into
     * the storage system.
     * @throws DAOException if an error occurred during the information
     * retrieving.
     *
     * @author Stefano Chirico
     * @since 1.0.0.190414
     */
    @Override
    public Prescription getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM \"public\".\"Prescriptions\"WHERE id = ?")) {
            stm.setInt(1, primaryKey);
            try (ResultSet rs = stm.executeQuery()) {

                rs.next();
                Prescription prescription = new Prescription();
                prescription.setId_prescription(rs.getInt("id_prescription"));
                prescription.setId_doctor(rs.getInt("id_doctor"));
                prescription.setId_patient(rs.getInt("id_patient"));
                prescription.setId_pharmacy(rs.getInt("id_pharmacy"));
                prescription.setDate(rs.getDate("date"));
                prescription.setDate_erog(rs.getDate("date_erog"));
                prescription.setDescription(rs.getString("description"));
                

                try (PreparedStatement todoStatement = CON.prepareStatement("SELECT count(*) FROM \"public\".\"Prescriptions\" WHERE id_patient = ?")) {
                    todoStatement.setInt(1, prescription.getId_patient());

                    ResultSet counter = todoStatement.executeQuery();
                    counter.next();
                   
                }

                return prescription;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }
    }

    /**
     * Returns the {@link User user} with the given {@code email} and
     * {@code password}.
     *
     * @param email the email of the user to get.
     * @param password the password of the user to get.
     * @return the {@link User user} with the given {@code email} and
     * {@code password}..
     * @throws DAOException if an error occurred during the information
     * retrieving.
     *
     * @author Stefano Chirico
     * @since 1.0.0.190414
     */
    
    @Override
    public List<Prescription> getAll() throws DAOException {
        List<Prescription> prescriptions = new ArrayList<>();
        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery("SELECT * FROM \"public\".\"Prescriptions\" ORDER BY date DESC")) 
            {              
                while (rs.next()) {
                    Prescription prescription = new Prescription();
                    prescription.setId_prescription(rs.getInt("id_prescription"));
                    prescription.setId_doctor(rs.getInt("id_doctor"));
                    prescription.setId_patient(rs.getInt("id_patient"));
                    prescription.setId_pharmacy(rs.getInt("id_pharmacy"));
                    prescription.setId_ticket(rs.getInt("id_ticket"));
                    prescription.setDate(rs.getDate("date"));
                    prescription.setDate_erog(rs.getDate("date_erog"));
                    prescription.setDescription(rs.getString("description"));
                    prescriptions.add(prescription);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }

        return prescriptions;
    }

    
    @Override
        public Prescription getLastPrescription(int id) throws DAOException {
        Prescription prescription = new Prescription();
        List<Prescription> prescriptions = new ArrayList<>();
        try (PreparedStatement std = CON.prepareStatement("SELECT * FROM \"public\".\"Prescriptions\" WHERE id_patient = ?")){
                std.setInt(1,id);
                ResultSet rs = std.executeQuery();
                while (rs.next()) {
                    Prescription p = new Prescription();
                    p.setId_prescription(rs.getInt("id_prescription"));
                    p.setId_doctor(rs.getInt("id_doctor"));
                    p.setId_patient(rs.getInt("id_patient"));
                    p.setId_pharmacy(rs.getInt("id_pharmacy"));
                    p.setId_ticket(rs.getInt("id_ticket"));
                    p.setDate(rs.getDate("date"));
                    p.setDate_erog(rs.getDate("date_erog"));
                    p.setDescription(rs.getString("description"));
                    prescriptions.add(p);
                }
        } catch (SQLException ex) {
            return null;
        }
        if(prescriptions.isEmpty()){
            return null;
        }
        if(prescriptions.size()==1){
            prescription = prescriptions.get(0);
        }
        else{
            prescription = prescriptions.get(0);
            for (int j = 1; j < prescriptions.size(); j++) {
                if(prescription.getDate().before(prescriptions.get(j).getDate())){
                    prescription = prescriptions.get(j);
                }
            }   
        }
        return prescription;
    }
        
    public List<Prescription> getByPharmacy(int id) throws DAOException{
        List<Prescription> prescriptions = new ArrayList<>();
        try (PreparedStatement std = CON.prepareStatement("SELECT * FROM \"public\".\"Prescriptions\" WHERE id_pharmacy = ?")){
                std.setInt(1, id);
                ResultSet rs = std.executeQuery();
                while (rs.next()) {
                    Prescription prescription = new Prescription();
                    prescription.setId_prescription(rs.getInt("id_prescription"));
                    prescription.setId_doctor(rs.getInt("id_doctor"));
                    prescription.setId_patient(rs.getInt("id_patient"));
                    prescription.setId_pharmacy(rs.getInt("id_pharmacy"));
                    prescription.setId_ticket(rs.getInt("id_ticket"));
                    prescription.setDate(rs.getDate("date"));
                    prescription.setDate_erog(rs.getDate("date_erog"));
                    prescription.setDescription(rs.getString("description"));;
                    prescriptions.add(prescription);
                }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
        return prescriptions;
    }
    
    @Override
    public List<Prescription> getByPatientId(int id) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Prescription getByTicketId(int id) throws DAOException {
        Prescription prescription = null;
        try (PreparedStatement std = CON.prepareStatement("SELECT * FROM \"public\".\"Prescriptions\" WHERE id_ticket = ?")){
            std.setInt(1, id);
            ResultSet rs = std.executeQuery();
            while (rs.next()) {
                prescription = new Prescription();
                prescription.setId_prescription(rs.getInt("id_prescription"));
                prescription.setId_doctor(rs.getInt("id_doctor"));
                prescription.setId_patient(rs.getInt("id_patient"));
                prescription.setId_pharmacy(rs.getInt("id_pharmacy"));
                prescription.setId_ticket(rs.getInt("id_ticket"));
                prescription.setDate(rs.getDate("date"));
                prescription.setDate_erog(rs.getDate("date_erog"));
                prescription.setDescription(rs.getString("description"));;
            }
        } catch (SQLException ex) {
            System.out.println(ex);
            //throw new DAOException("Impossible to get the list of users", ex);
        }

        return prescription;
    }
    
    public void addPrescription(int id_patient, int id_doctor, String description, Date date) throws DAOException {
        Random r = new Random();
        try (PreparedStatement std = CON.prepareStatement("INSERT INTO\"public\".\"Prescriptions\""
                + "(id_patient, id_doctor, description, date, id_prescription)"
                + "VALUES (?, ?, ?, ?, ?);")) {
            
            
            std.setInt(1, id_patient);
            std.setInt(2, id_doctor);
            std.setString(3, description);
            std.setDate(4, date);
            int size = getCount().intValue();
            size++;
            std.setInt(5, size);
            
            
            /*
            JDBCTicketDAO ticketDAO = new JDBCTicketDAO(CON);
            int value = r.nextInt((50 - 10) + 1) + 10;
            ticketDAO.createnewTicket(value,date);
            */
            
            std.executeUpdate();
        
        }catch (SQLException ex) {
            Logger.getLogger(JDBCPrescriptionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }

   
}
