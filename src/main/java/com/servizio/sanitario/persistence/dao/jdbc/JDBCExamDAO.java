/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.dao.jdbc;

import com.servizio.sanitario.persistence.dao.ExamDAO;
import com.servizio.sanitario.persistence.entities.Exam;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Pretto Stefano
 */
public class JDBCExamDAO extends JDBCDAO<Exam, Integer> implements ExamDAO {

    public JDBCExamDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM public.\"Exams\"");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count users", ex);
        }

        return 0L;
    }

    @Override
    public void createnewExam(int id_patient, int id_doctor, int id_specialized_doctor, int id_prescribable, Date date_exam) throws DAOException {
        try (PreparedStatement stm = CON.prepareStatement("INSERT INTO public.\"Exams\""
                + "(id_patient, id_doctor, id_specialized_doctor, id_prescribable, date_exam, id_ticket)"
                + "VALUES (?, ?, ?, ?, ?, ?);")) {
            stm.setInt(1, id_patient);
            stm.setInt(2, id_doctor);
            stm.setInt(3, id_specialized_doctor);
            stm.setInt(4, id_prescribable);
            stm.setDate(5, date_exam);
            //crea un nuovo ticket di valore pari a 50 euro
            
            JDBCTicketDAO ticketDAO = new JDBCTicketDAO(CON);
            int id_ticket = ticketDAO.createnewTicket(50);
            stm.setInt(6, id_ticket);
            
            stm.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

    @Override
    public List<Exam> getAll() throws DAOException {
        List<Exam> exams = new ArrayList<>();
        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery("SELECT * FROM \"public\".\"Exams\"")) {
                while (rs.next()) {
                    Exam exam = new Exam();
                    exam.setId_patient(rs.getInt("id_patient"));
                    exam.setId_doctor(rs.getInt("id_doctor"));
                    exam.setId_specialized_doctor(rs.getInt("id_specialized_doctor"));
                    exam.setId_ticket(rs.getInt("id_ticket"));
                    exam.setId_prescribable(rs.getInt("id_prescribable"));
                    exam.setDate_exam(rs.getDate("date_exam"));
                    exam.setResult(rs.getString("result"));
                    exams.add(exam);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }

        return exams;
    }

    @Override
    public List<Exam> getAllByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }
        List<Exam> exams = new ArrayList<>();
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM \"public\".\"Exams\"WHERE id_patient = "+primaryKey + "ORDER BY date_exam DESC")) {

            //stm.setInt(1, primaryKey);
            try (ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Exam exam = new Exam();

                    exam.setId_patient(rs.getInt("id_patient"));
                    exam.setId_doctor(rs.getInt("id_doctor"));
                    exam.setId_specialized_doctor(rs.getInt("id_specialized_doctor"));
                    exam.setId_ticket(rs.getInt("id_ticket"));
                    exam.setId_prescribable(rs.getInt("id_prescribable"));
                    exam.setDate_exam(rs.getDate("date_exam"));
                    exam.setResult(rs.getString("result"));
                    exam.setDone(rs.getBoolean("done"));
                    exams.add(exam);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }

        return exams;
    }

    @Override
    public Exam getLastExam(int id) throws DAOException {
        Exam exam = null;
        List<Exam> exams = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM \"public\".\"Exams\" WHERE id_patient = ?;")) {
            stm.setInt(1, id);
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Exam p = new Exam();
                p.setDate_exam(rs.getDate("date_exam"));
                p.setId_doctor(rs.getInt("id_doctor"));
                p.setId_patient(rs.getInt("id_patient"));
                p.setId_prescribable(rs.getInt("id_prescribable"));
                p.setId_specialized_doctor(rs.getInt("id_specialized_doctor"));
                p.setId_ticket(rs.getInt("id_ticket"));
                p.setResult(rs.getString("result"));
                exams.add(p);
            }
        } catch (SQLException ex) {
            return null;
        }

        if (exams.size() == 1) {
            exam = exams.get(0);
        } else {
            exam = exams.get(0);
            for (int j = 1; j < exams.size(); j++) {
                if (exam.getDate_exam().before(exams.get(j).getDate_exam())) {
                    exam = exams.get(j);
                }
            }
        }
        return exam;
    }

    @Override
    public Exam getByPrimaryKey(Integer arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addResult(String result, int id_patient, int id_doctor, int id_specialized_doctor, Date date_exam) throws DAOException {
        System.out.println("AddResult");
        try (PreparedStatement stm = CON.prepareStatement("UPDATE \"public\".\"Exams\" SET result = ?, done=true WHERE id_patient = ? AND id_doctor = ? AND id_specialized_doctor = ? AND date_exam = ?;")) {
            stm.setString(1, result);
            stm.setInt(2, id_patient);
            stm.setInt(3, id_doctor);
            stm.setInt(4, id_specialized_doctor);
            stm.setDate(5, date_exam);
            stm.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Errore");
        }
    }

    @Override
    public Exam getByTicketId(int id) throws DAOException {
        Exam exam = null;

        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM \"public\".\"Exams\" WHERE id_ticket = ?")) {
            stm.setInt(1, id);
            try (ResultSet rs = stm.executeQuery()) {
                while(rs.next()){
                    exam = new Exam();
                    exam.setId_patient(rs.getInt("id_patient"));
                    exam.setId_doctor(rs.getInt("id_doctor"));
                    exam.setId_specialized_doctor(rs.getInt("id_specialized_doctor"));
                    exam.setId_ticket(rs.getInt("id_ticket"));
                    exam.setId_prescribable(rs.getInt("id_prescribable"));
                    exam.setDate_exam(rs.getDate("date_exam"));
                    exam.setResult(rs.getString("result"));
                    exam.setDone(rs.getBoolean("done"));
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }

        return exam;
    }

}
