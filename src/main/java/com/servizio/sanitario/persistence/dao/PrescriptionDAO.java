/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.dao;

import com.servizio.sanitario.persistence.entities.Prescription;
import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import com.servizio.sanitario.persistence.entities.User;
import java.sql.Date;
import java.util.List;

/**
 * All concrete DAOs must implement this interface to handle the persistence 
 * system that interact with {@link User users}.
 * 
 * @author Stefano Chirico &lt;stefano dot chirico at unitn dot it&gt;
 * @since 2019.04.14
 */
public interface PrescriptionDAO extends DAO<Prescription, Integer> {
    public List<Prescription> getAll() throws DAOException;
    public List<Prescription> getByPatientId(int id) throws DAOException;
    public Prescription getByPrimaryKey(Integer primaryKey) throws DAOException;
    public Prescription getLastPrescription(int id) throws DAOException;
    public List<Prescription> getByPharmacy(int id) throws DAOException;
    public Prescription getByTicketId(int id) throws DAOException;
    public void addPrescription(int id_patient, int id_doctor, String description, Date date) throws DAOException;
}

