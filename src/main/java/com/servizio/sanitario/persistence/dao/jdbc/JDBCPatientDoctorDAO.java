/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.dao.jdbc;

import com.servizio.sanitario.persistence.dao.PatientDoctorDAO;
import com.servizio.sanitario.persistence.entities.Patient_Doctor;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pretto Stefano
 */
public class JDBCPatientDoctorDAO extends JDBCDAO<Patient_Doctor, Integer> implements PatientDoctorDAO {

    public JDBCPatientDoctorDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM \"public\".\"Patient_Doctor\"");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count users", ex);
        }

        return 0L;
    }

    public Patient_Doctor getByPrimaryKey(Integer key) throws DAOException {
        return null;

    }

    @Override
    public List<Patient_Doctor> getAll() throws DAOException {
        List<Patient_Doctor> patient_Doctors = new ArrayList<>();

        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery("SELECT * FROM \"public\".\"Patient_Doctor\"")) {
                while (rs.next()) {

                    Patient_Doctor patient_Doctor = new Patient_Doctor();
                    patient_Doctor.setId_doctor(rs.getInt("id_doctor"));
                    patient_Doctor.setId_patient(rs.getInt("id_patient"));
                    patient_Doctors.add(patient_Doctor);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }

        return patient_Doctors;
    }

    @Override
    public Integer getDoctorId(int id_patient) throws DAOException {
        Integer id_doctor = null;
        try (PreparedStatement stm = CON.prepareStatement("SELECT id_doctor FROM \"public\".\"Patient_doctor\" where id_patient = ?;")) {
            stm.setInt(1, id_patient);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {

                id_doctor = rs.getInt("id_doctor");
                return id_doctor;
            }
        } catch (SQLException ex) {
            Logger.getLogger(JDBCPatientDoctorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }


    public void updateDoctorId(int Id_Patient, int newId_doctor) throws DAOException{
        try (PreparedStatement stm = CON.prepareStatement("UPDATE \"public\".\"Patient_doctor\" SET id_doctor=? WHERE id_patient=? ")) {           
            stm.setInt(1, newId_doctor);
            stm.setInt(2, Id_Patient);
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }    
    }
    
  
}
