/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.dao.jdbc;

import com.servizio.sanitario.persistence.dao.TicketDAO;
import com.servizio.sanitario.persistence.entities.Ticket;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class JDBCTicketDAO extends JDBCDAO<Ticket, Integer> implements TicketDAO {

    public JDBCTicketDAO(Connection con) {
        super(con);
    }
    
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM \"public\".\"Tickets\"");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count users", ex);
        }

        return 0L;
    }

    @Override
    public Ticket getByPrimaryKey(Integer arg0) throws DAOException {
        Ticket ticket = null;
        try(PreparedStatement stm = CON.prepareStatement("SELECT * FROM \"public\".\"Tickets\" WHERE id_ticket = ?")){
           stm.setInt(1, arg0);
           try (ResultSet rs = stm.executeQuery()) {
               rs.next();
               ticket = new Ticket();
               ticket.setId_ticket(rs.getInt("id_ticket"));
               ticket.setValue(rs.getInt("value"));
               ticket.setPaid(rs.getBoolean("paid"));
               ticket.setTimestamp(rs.getDate("timestamp"));
           }
        }catch (SQLException ex) {
            return null;
        }

        return ticket;
    }

    @Override
    public List<Ticket> getAll() throws DAOException {
        List<Ticket> tickets = new ArrayList<>();
        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery("SELECT * FROM \"public\".\"Tickets\"")) 
            {              
                while (rs.next()) {
                    Ticket ticket = new Ticket();
                    ticket.setId_ticket(rs.getInt("id_ticket"));
                    ticket.setValue(rs.getInt("value"));
                    ticket.setPaid(rs.getBoolean("paid"));
                    ticket.setExam(rs.getBoolean("exam"));
                    ticket.setTimestamp(rs.getDate("timestamp"));
                    tickets.add(ticket);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }

        return tickets;
    }
    
    public void updatePaid(Integer id){
        try (PreparedStatement stm = CON.prepareStatement("UPDATE \"public\".\"Tickets\" SET paid=? WHERE id_ticket=?;")){
            stm.setBoolean(1, true);
            stm.setInt(2, id);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public int createnewTicket(int value) throws DAOException {
        int size = 0;
        try (PreparedStatement stm = CON.prepareStatement("INSERT INTO public.\"Tickets\""
                + "(id_ticket, value, exam, paid)"
                + "VALUES (?, ?, ?, ?);")) {
            
            try (PreparedStatement stm1 = CON.prepareStatement("SELECT MAX(id_ticket) AS size FROM \"public\".\"Tickets\"")) {
                try (ResultSet rs = stm1.executeQuery()) {
                    rs.next();
                    size = Integer.parseInt(rs.getString("size"));
                }
            } catch (SQLException ex) {
                System.out.println("TIcket non trovato");
            }
            size++;
            stm.setInt(1,size);
            stm.setInt(2, value);
            stm.setBoolean(3, true);
            stm.setBoolean(4, false);
            stm.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return size;
    }
    
}
