/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.dao;

import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import com.servizio.sanitario.persistence.entities.User;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * All concrete DAOs must implement this interface to handle the persistence 
 * system that interact with {@link User users}.
 * 
 * @author Stefano Chirico &lt;stefano dot chirico at unitn dot it&gt;
 * @since 2019.04.14
 */
public interface UserDAO extends DAO<User, Integer> {
    /**
     * Returns the {@link User user} with the given {@code email} and
     * {@code password}.
     * @param email the email of the user to get.
     * @param password the password of the user to get.
     * @return the {@link User user} with the given {@code username} and
     * {@code password}..
     * @throws DAOException if an error occurred during the information
     * retrieving.
     * 
     * @author Stefano Chirico
     * @since 1.0.0.190414
     */
    public User getByEmailAndPassword(String email, String password) throws DAOException;
    public List<User> getAll() throws DAOException;
    public List<User> getAllforDoctor(int id) throws DAOException;
    public List<User> getPatientDoctor(int id) throws DAOException;
    public String getByEmail(String email) throws DAOException;
    public User getPrimary(int primaryKey) throws DAOException;
    public List<User> getPatient(int id) throws DAOException;
    public String  updateToken(String email, String password) throws DAOException;
    public User getByToken(String token) throws DAOException, SQLException;
    public void updatePhoto(int userId, String fileName ,String path) throws DAOException, IOException;
    public int getCardinalityFile(int user_id) throws Exception;
    public int getCardinalityPath(int user_id) throws Exception;
    public String getFileName(int index, int userId) throws Exception;
    public void deletePhoto(int user_id) throws Exception;
    public byte[] getSalt(String email);
    public void updatePsw(String psw_criptata, byte[] salt, String email);
    public String getPathImgs(int id_user, int index);
    public void db();
    public void updateCF(String CF, int id) throws  DAOException, IOException;
}

