/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.dao.jdbc;

import com.servizio.sanitario.persistence.dao.PharmacyDAO;
import com.servizio.sanitario.persistence.entities.Pharmacy;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class JDBCPharmacyDAO extends JDBCDAO<Pharmacy, Integer> implements PharmacyDAO {

    
    public JDBCPharmacyDAO(Connection con) {
        super(con);
    }
    
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM \"public\".\"Pharmacies\"");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count Pharmacies", ex);
        }

        return 0L;    
    }

    @Override
    public List<Pharmacy> getAll() throws DAOException {
        List<Pharmacy> pharmacies = new ArrayList<>();
        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery("SELECT * FROM \"public\".\"Pharmacies\"")) 
            {              
                while (rs.next()) {
                    Pharmacy pharmacy = new Pharmacy();
                    pharmacy.setId(rs.getInt("id"));
                    pharmacy.setName(rs.getString("name"));
                    pharmacy.setProvince(rs.getString("province"));
                    pharmacies.add(pharmacy);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
        
        return pharmacies;
    }

    @Override
    public List<Pharmacy> getByProvince(String province) throws DAOException {
        List<Pharmacy> pharmacies = new ArrayList<>();
        try (PreparedStatement std = CON.prepareStatement("SELECT * FROM \"public\".\"Pharmacies\" WHERE province = ?")){
                std.setString(1, province);
                ResultSet rs = std.executeQuery();
                while (rs.next()) {
                    Pharmacy pharmacy = new Pharmacy();
                    pharmacy.setId(rs.getInt("id"));
                    pharmacy.setName(rs.getString("name"));
                    pharmacy.setProvince(rs.getString("province"));
                    pharmacies.add(pharmacy);
                }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
        return pharmacies;
    }

    @Override
    public Pharmacy getByPrimaryKey(Integer arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
