/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.dao;

import com.servizio.sanitario.persistence.entities.Ticket;
import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author asus
 */
public interface TicketDAO extends DAO<Ticket, Integer> {
    public List<Ticket> getAll() throws DAOException;
    public Ticket getByPrimaryKey(Integer arg0) throws DAOException;
    public void updatePaid(Integer id);
    public int createnewTicket(int value) throws DAOException;
    
}
