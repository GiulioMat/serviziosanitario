/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.dao.jdbc;

import com.servizio.sanitario.persistence.dao.PrescribableDAO;
import com.servizio.sanitario.persistence.entities.Prescribable;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author asus
 */
public class JDBCPrescribableDAO extends JDBCDAO<Prescribable, Integer> implements PrescribableDAO {
    public JDBCPrescribableDAO(Connection con) {
        super(con);
    }
     
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM \"public\".\"Prescribable_tests\"");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count Prescribable_tests", ex);
        }

        return 0L;      
    }

    @Override
    public Prescribable getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM \"public\".\"Prescribable_tests\"WHERE id_prescribable = ?")) {
            stm.setInt(1, primaryKey);
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Prescribable prescribable = new Prescribable();
                prescribable.setId_prescribable(rs.getInt("id_prescribable"));
                prescribable.setName(rs.getString("name"));
                prescribable.setDescription(rs.getString("description"));

                return prescribable;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }
    }

    @Override
    public List<Prescribable> getAll() throws DAOException {
        List<Prescribable> prescribables = new ArrayList<>();
        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery("SELECT * FROM \"public\".\"Prescribable_tests\"")) 
            {
                while (rs.next()) {
                    Prescribable prescribable = new Prescribable();
                    prescribable.setId_prescribable(rs.getInt("id_prescribable"));
                    prescribable.setName(rs.getString("name"));
                    prescribable.setDescription(rs.getString("description"));
                    prescribables.add(prescribable);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
        return prescribables;
    }    
    
}
