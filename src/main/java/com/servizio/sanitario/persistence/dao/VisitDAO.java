/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.dao;

import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import com.servizio.sanitario.persistence.entities.User;
import com.servizio.sanitario.persistence.entities.Visit;
import java.sql.Date;
import java.util.List;

/**
 * All concrete DAOs must implement this interface to handle the persistence 
 * system that interact with {@link User users}.
 * 
 * @author Stefano Chirico &lt;stefano dot chirico at unitn dot it&gt;
 * @since 2019.04.14
 */
public interface VisitDAO extends DAO<Visit, Integer> {
    public List<Visit> getAll() throws DAOException;
    public List<Visit> getAllByPatient(Integer primaryKey) throws DAOException;
    public List<Visit> getAllByDoctor(Integer primaryKey) throws DAOException;
    public void createnewVisit(int id_patient,int id_doctor, Date date) throws DAOException;
}

