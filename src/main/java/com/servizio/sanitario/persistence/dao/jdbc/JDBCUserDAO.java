/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.dao.jdbc;

import com.servizio.sanitario.persistence.dao.UserDAO;
import com.servizio.sanitario.persistence.entities.User;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The JDBC implementation of the {@link UserDAO} interface.
 *
 * @author Stefano Chirico &lt;stefano dot chirico at unitn dot it&gt;
 * @since 2019.04.14
 */
public class JDBCUserDAO extends JDBCDAO<User, Integer> implements UserDAO {

    /**
     * The default constructor of the class.
     *
     * @param con the connection to the persistence system.
     *
     * @author Stefano Chirico
     * @since 1.0.0.190414
     */
    public JDBCUserDAO(Connection con) {
        super(con);
    }

    /**
     * Returns the number of {@link User users} stored on the persistence system
     * of the application.
     *
     * @return the number of records present into the storage system.
     * @throws DAOException if an error occurred during the information
     * retrieving.
     *
     * @author Stefano Chirico
     * @since 1.0.0.190414
     */
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM \"public\".\"Users\"");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count users", ex);
        }

        return 0L;
    }

    /**
     * Returns the {@link User user} with the primary key equals to the one
     * passed as parameter.
     *
     * @param primaryKey the {@code id} of the {@code user} to get.
     * @return the {@code user} with the id equals to the one passed as
     * parameter or {@code null} if no entities with that id are present into
     * the storage system.
     * @throws DAOException if an error occurred during the information
     * retrieving.
     *
     * @author Stefano Chirico
     * @since 1.0.0.190414
     */
    @Override
    public User getPrimary(int primaryKey) throws DAOException {
        User user = null;
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM \"public\".\"Users\" WHERE id_user = ?")) {
            stm.setInt(1, primaryKey);
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                user = new User();
                user.setId(rs.getInt("id_user"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("psw"));
                user.setFirstName(rs.getString("name"));
                user.setSurname(rs.getString("surname"));
                user.setCf(rs.getString("cf"));
                user.setDate_of_birth(rs.getDate("date_of_birth"));
                user.setProvince(rs.getString("province"));
                user.setRole(rs.getString("role"));
                user.setToken(rs.getString("token"));
                return user;
            }
        } catch (SQLException ex) {
            System.out.println("Utente non trovato");
        }
        return user;
    }

    @Override
    public List<User> getPatient(int id) throws DAOException {
        List<User> users = new ArrayList<>();

        try (PreparedStatement std = CON.prepareStatement("SELECT * FROM \"public\".\"Users\" INNER JOIN \"public\".\"Patient_doctor\" ON \"public\".\"Users\".id_user = \"public\".\"Patient_doctor\".id_patient WHERE id_doctor=?")) {
            std.setInt(1, id);
            ResultSet rs = std.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id_user"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("psw"));
                user.setFirstName(rs.getString("name"));
                user.setSurname(rs.getString("surname"));
                users.add(user);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
        return users;

    }

    /**
     * Returns the {@link User user} with the given {@code email} and
     * {@code password}.
     *
     * @param email the email of the user to get.
     * @param password the password of the user to get.
     * @return the {@link User user} with the given {@code email} and
     * {@code password}..
     * @throws DAOException if an error occurred during the information
     * retrieving.
     *
     * @author Stefano Chirico
     * @since 1.0.0.190414
     */

    @Override
    public User getByEmailAndPassword(String email, String password) throws DAOException {
        if ((email == null) || (password == null)) {
            throw new DAOException("Email and password are mandatory fields", new NullPointerException("email or password are null"));
        }
        //System.out.println("Email: " +email);
        //System.out.println("PSW: " +password +"\n\n");
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM \"Users\" WHERE email = ? AND psw = ?")) {
            stm.setString(1, email);
            stm.setString(2, password);
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                User user = new User();
                user.setId(rs.getInt("id_user"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("psw"));
                user.setFirstName(rs.getString("name"));
                user.setSurname(rs.getString("surname"));
                user.setCf(rs.getString("cf"));
                user.setRole(rs.getString("role"));
                user.setDate_of_birth(rs.getDate("date_of_birth"));
                user.setProvince(rs.getString("province"));
                return user;
            } catch (SQLException ex) {
                System.out.println("nessun user trovato");
            }

            return null;
        } catch (SQLException ex) {
            Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Returns the list of all the valid {@link User users} stored by the
     * storage system.
     *
     * @return the list of all the valid {@code users}.
     * @throws DAOException if an error occurred during the information
     * retrieving.
     *
     * @author Stefano Chirico
     * @since 1.0.0.190414
     */

    @Override
    public List<User> getAll() throws DAOException {

        List<User> users = new ArrayList<>();

        try (Statement stm = CON.createStatement()) {

            try (ResultSet rs = stm.executeQuery("SELECT * FROM \"public\".\"Users\"")) {
                //arriva fino a qua
                while (rs.next()) {
                    User user = new User();
                    user.setId(rs.getInt("id_user"));
                    user.setEmail(rs.getString("email"));
                    user.setPassword(rs.getString("psw"));
                    user.setFirstName(rs.getString("name"));
                    user.setSurname(rs.getString("surname"));
                    user.setProvince(rs.getString("province"));
                    user.setRole(rs.getString("role"));
                    user.setDate_of_birth(rs.getDate("date_of_birth"));
                    user.setSex(rs.getString("sex"));
                    users.add(user);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }

        return users;
    }

    @Override
    public String getByEmail(String email) throws DAOException {
        String mail = "";
        try (PreparedStatement std = CON.prepareStatement("SELECT * FROM \"public\".\"Users\" WHERE email = ?")) {
            std.setString(1, email);
            ResultSet rs = std.executeQuery();
            rs.next();
            mail = rs.getString("email");
            mail += "-" + rs.getString("name");

        } catch (SQLException ex) {
            return null;
        }
        return mail;
    }

    public String updateToken(String email, String password) throws DAOException {
        String token = getAlphaNumericString(30);

        try (PreparedStatement std = CON.prepareStatement("UPDATE \"public\".\"Users\" SET token = ? WHERE email = ? AND psw = ?")) {
            std.setString(1, token);
            std.setString(2, email);
            std.setString(3, password);
            std.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return token;
    }

    public User getByToken(String token) throws DAOException, SQLException {
        User user = new User();
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM \"public\".\"Users\" WHERE token = ?")) {
            stm.setString(1, token);
            try (ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    user.setId(rs.getInt("id_user"));
                    user.setEmail(rs.getString("email"));
                    user.setPassword(rs.getString("psw"));
                    user.setFirstName(rs.getString("name"));
                    user.setSurname(rs.getString("surname"));
                    user.setCf(rs.getString("cf"));
                    user.setRole(rs.getString("role"));
                    user.setDate_of_birth(rs.getDate("date_of_birth"));
                    user.setProvince(rs.getString("province"));

                }
            } catch (SQLException ex) {
                System.out.println(ex);
            }
        }
        return user;
    }

    private String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {
            int index = (int) (AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }
        return sb.toString();
    }

    public void updatePhoto(int userId, String fileName, String path) throws DAOException, IOException {
        int cardinality_fileName = 0;
        int cardinality_pathImgs = 0;
        try {
            cardinality_fileName = getCardinalityFile(userId);
            cardinality_pathImgs = getCardinalityPath(userId);
        } catch (Exception ex) {
            Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        try (PreparedStatement ps = CON.prepareStatement("UPDATE \"public\".\"Users\" SET filename_imgs[?] = ? , path_imgs[?] = ? WHERE id_user=?")) {
            ps.setInt(1, cardinality_fileName);
            ps.setString(2, fileName);
            ps.setInt(3, cardinality_pathImgs);
            ps.setString(4, path);
            ps.setInt(5, userId);

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getCardinalityFile(int user_id) throws Exception {
        int cardinality = 0;
        try (PreparedStatement ps = CON.prepareStatement("SELECT cardinality(filename_imgs) AS result FROM \"public\".\"Users\" WHERE id_user=?")) {
            ps.setInt(1, user_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                cardinality = rs.getInt("result");
                return cardinality;
            }
        } catch (Exception e) {
        }
        return cardinality;
    }

    public int getCardinalityPath(int user_id) throws Exception {
        int cardinality = 0;
        try (PreparedStatement ps = CON.prepareStatement("SELECT cardinality(path_imgs) AS result FROM \"public\".\"Users\" WHERE id_user=?")) {
            ps.setInt(1, user_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                cardinality = rs.getInt("result");
                return cardinality;
            }
        } catch (Exception e) {
        }
        return cardinality;
    }

    public String getFileName(int index, int userId) throws Exception {
        try (PreparedStatement ps = CON.prepareStatement("SELECT filename_imgs[?] AS file FROM public.\"Users\" WHERE id_user=?")) {
            ps.setInt(1, index);
            ps.setInt(2, userId);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                return rs.getString("file");
            }
        }
        return null;
    }

    public void deletePhoto(int user_id) throws Exception {
        try (PreparedStatement ps = CON.prepareStatement("UPDATE \"public\".\"Users\" SET path_imgs= null , filename_imgs= null WHERE id_user= ?")) {
            ps.setInt(1, user_id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public byte[] getSalt(String email) {
        byte[] salt = null;
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM \"public\".\"Users\" WHERE email = ?")) {
            stm.setString(1, email);
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    salt = rs.getBytes("salt");
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return salt;
    }

    public String getPathImgs(int id_user, int index) {
        String path = null;
        try (PreparedStatement stm = CON.prepareStatement("SELECT path_imgs[?] AS path FROM \"public\".\"Users\" WHERE id_user = ?")) {
            stm.setInt(1, index);
            stm.setInt(2, id_user);
            try (ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    path = rs.getString("path");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return path;

    }

    public void updatePsw(String psw_criptata, byte[] salt, String email) {
        try (PreparedStatement stm = CON.prepareStatement("UPDATE public.\"Users\" SET psw=?, salt=? WHERE email=?;")) {
            stm.setString(1, psw_criptata);
            stm.setBytes(2, salt);
            stm.setString(3, (String) email);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<User> getAllforDoctor(int id) throws DAOException {
        List<User> users = new ArrayList<>();

        try (PreparedStatement std = CON.prepareStatement("SELECT * FROM \"public\".\"Users\" WHERE id_user != ?")) {
            std.setInt(1, id);
            ResultSet rs = std.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id_user"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("psw"));
                user.setFirstName(rs.getString("name"));
                user.setSurname(rs.getString("surname"));
                user.setRole(rs.getString("role"));
                user.setProvince(rs.getString("province"));
                users.add(user);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
        return users;
    }

    @Override
    public List<User> getPatientDoctor(int id) throws DAOException {
        List<User> users = new ArrayList<>();

        try (PreparedStatement std = CON.prepareStatement("SELECT * FROM public.\"Users\" AS u INNER JOIN public.\"Patient_doctor\" AS p ON u.id_user = p.id_patient WHERE id_doctor = ?")) {
            std.setInt(1, id);
            ResultSet rs = std.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id_user"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("psw"));
                user.setFirstName(rs.getString("name"));
                user.setSurname(rs.getString("surname"));
                user.setRole(rs.getString("role"));
                user.setProvince(rs.getString("province"));
                users.add(user);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
        return users;
    }
    
    public void updateCF(String CF, int id) throws DAOException, IOException {
        try (PreparedStatement stm = CON.prepareStatement("UPDATE public.\"Users\" SET cf=? WHERE id_user=?;")) {
            stm.setString(1, CF);
            stm.setInt(2, id);

            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void db() {
        /*
        Random r = new Random();
        
        Faker faker = new Faker(new Locale("it"));
        String[] province={"Trento", "Vicenza"};
        String[] city_TN = {"Povo","Rovereto", "Ala", "Andalo", "Arco", "Civezzano", "Mori"};
        String[] city_VI = {"Sovizzo", "Altavilla", "Creazzo", "Montecchio Maggiore", "Arzignano", "Montorso"};
        
        String[] sex={"M", "F"};
        
        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = dateformat.parse("01/01/1914");
            d2 = dateformat.parse("01/01/2019");
        } catch (ParseException ex) {
            Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }   
        
        File folder = new File("D:\\Documents\\Università\\2o Anno\\Programmazione per il Web\\Progetto\\nuovo\\serviziosanitario\\src\\main\\webapp\\images");
        File[] listOfFiles = folder.listFiles();
        ArrayList<String> photos = new ArrayList<>();
        for (File file : listOfFiles) {
            if (file.isFile()) {
                photos.add(file.getName());
            }
        }
        
        int index=0;
        
        for(int i=1; i<6; i++){
            String nome = faker.name().firstName();
            String cognome = faker.name().lastName();
            Date date = new Date(ThreadLocalRandom.current().nextLong(d1.getTime(), d2.getTime()));
            String prov = province[r.nextInt(province.length)];
            
            try (PreparedStatement stm = CON.prepareStatement("INSERT INTO public.\"Users\"(\n" +
                "	role, name, surname, cf, email, province, id_user, sex, date_of_birth, city)\n" +
                "	VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);")){
                stm.setString(1, "MedicoBase");
                stm.setString(2, nome);
                stm.setString(3, cognome);
                stm.setString(4, getAlphaNumericString(16).toUpperCase());
                stm.setString(5, nome.toLowerCase() + "." + cognome.toLowerCase() + "@gmail.com");
                stm.setString(6, prov);
                stm.setInt(7, i);
                stm.setString(8, sex[r.nextInt(province.length)]);
                stm.setDate(9 , new java.sql.Date(date.getTime()));
                
                if(prov.equals("Trento"))
                    stm.setString(10, city_TN[r.nextInt(city_TN.length)]);
                else
                    stm.setString(10, city_VI[r.nextInt(city_VI.length)]);
                stm.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        for(int i=6; i<57; i++){
            String nome = faker.name().firstName();
            String cognome = faker.name().lastName();
            Date date = new Date(ThreadLocalRandom.current().nextLong(d1.getTime(), d2.getTime()));
            String prov = province[r.nextInt(province.length)];
            
            try (PreparedStatement stm = CON.prepareStatement("INSERT INTO public.\"Users\"(\n" +
                "	role, name, surname, cf, email, province, id_user, sex, date_of_birth, city, path_imgs[?], filename_imgs[?])\n" +
                "	VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ?);")){
                stm.setInt(1, getCardinalityPath(i));
                stm.setInt(2, getCardinalityPath(i));
                stm.setString(3, "paziente");
                stm.setString(4, nome);
                stm.setString(5, cognome);
                stm.setString(6, getAlphaNumericString(16).toUpperCase());
                stm.setString(7, nome.toLowerCase() + "." + cognome.toLowerCase() + "@gmail.com");
                stm.setString(8, prov);
                stm.setInt(9, i);
                stm.setString(10, sex[r.nextInt(province.length)]);
                stm.setDate(11 , new java.sql.Date(date.getTime()));
                if(prov.equals("Trento"))
                    stm.setString(12, city_TN[r.nextInt(city_TN.length)]);
                else
                    stm.setString(12, city_VI[r.nextInt(city_VI.length)]);
                
                stm.setString(13, "D:\\Documents\\Università\\2o Anno\\Programmazione per il Web\\Progetto\\nuovo\\serviziosanitario\\src\\main\\webapp\\images\\" + photos.get(index));
                stm.setString(14, photos.get(index));
                index++;
                
                stm.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        for(int i=57; i<62; i++){
            String nome = faker.name().firstName();
            String cognome = faker.name().lastName();
            Date date = new Date(ThreadLocalRandom.current().nextLong(d1.getTime(), d2.getTime()));
            String prov = province[r.nextInt(province.length)];
            
            try (PreparedStatement stm = CON.prepareStatement("INSERT INTO public.\"Users\"(\n" +
                "	role, name, surname, cf, email, province, id_user, sex, date_of_birth, city)\n" +
                "	VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);")){
                stm.setString(1, "MedicoSpecialista");
                stm.setString(2, nome);
                stm.setString(3, cognome);
                stm.setString(4, getAlphaNumericString(16).toUpperCase());
                stm.setString(5, nome.toLowerCase() + "." + cognome.toLowerCase() + "@gmail.com");
                stm.setString(6, prov);
                stm.setInt(7, i);
                stm.setString(8, sex[r.nextInt(province.length)]);
                stm.setDate(9 , new java.sql.Date(date.getTime()));
                
                if(prov.equals("Trento"))
                    stm.setString(10, city_TN[r.nextInt(city_TN.length)]);
                else
                    stm.setString(10, city_VI[r.nextInt(city_VI.length)]);
                
                stm.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        int index1=0;
        for(int i=62; i<62+province.length; i++){
            try (PreparedStatement stm = CON.prepareStatement("INSERT INTO public.\"Users\"(\n" +
                    "	role, email, province, id_user)\n" +
                    "	VALUES (?, ?, ?, ?);")){
                    stm.setString(1, "SSP");
                    stm.setString(2, province[index1] + ".servizio@gmail.com");
                    stm.setString(3, province[index1]);
                    stm.setInt(4, i);
                    
                    stm.executeUpdate();
                } catch (SQLException ex) {
                    Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            index1++;    
        }
        
        //Tabella Patient-Doctor
        for(int i=6; i<56; i++){
            try (PreparedStatement stm = CON.prepareStatement("INSERT INTO public.\"Patient_doctor\"(\n" +
                    "	id_patient, id_doctor)\n" +
                    "	VALUES (?, ?);")){
                stm.setInt(1, i);
                //provincia del paziente con id_user=i
                String provincia = getPrimary(i).province;
                
                //prende tutti i dottori di quella provincia
                List<Integer> id_doctors = new ArrayList<>();
                try (PreparedStatement stm1 = CON.prepareStatement("SELECT id_user FROM public.\"Users\" WHERE province = ? AND role = ?")){
                    stm1.setString(1, provincia);
                    stm1.setString(2, "MedicoBase");
                    
                    try (ResultSet rs = stm1.executeQuery()) {
                        while (rs.next()) {
                            id_doctors.add(rs.getInt("id_user"));
                        }                                      
                    }  
                }
                
               
                //prendo un dottore casuale di quella provincia
                if(!id_doctors.isEmpty()) {
                    stm.setInt(2, id_doctors.get(r.nextInt(id_doctors.size())));
                    stm.executeUpdate();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
            } catch (DAOException ex) {
                Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        for(int i=0;i<62+province.length;i++){
            String psw_criptata = null;
            byte[] salt = new byte[16];
            String email = null;
            try {
                email = getPrimary(i).email;
                SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
                sr.nextBytes(salt);
                psw_criptata = get_SHA_256_SecurePassword("123", salt);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(ChangePasswordServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (DAOException ex) {
                Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            updatePsw(psw_criptata,salt,(String)email);
        }
        
    }
    
    private static String get_SHA_256_SecurePassword(String passwordToHash, byte[] salt)
    {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(salt);
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        
        return generatedPassword;
    
         */
 /*
        //Exams
        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String prova = now.format(formatter);
        Date d3 = null;
        Date d4 = null;
        
        
        try {
            d3 = dateformat.parse(prova);
            d4 = dateformat.parse("01/01/2022");
        } catch (ParseException ex) {
            Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
        for(int i=0;i<50;i++){
            Date date = new Date(ThreadLocalRandom.current().nextLong(d3.getTime(), d4.getTime()));
            try {  
                List<Patient_Doctor> patient_Doctors = new ArrayList<>();
                try (Statement stm = CON.createStatement()) {
                    try (ResultSet rs = stm.executeQuery("SELECT * FROM \"public\".\"Patient_Doctor\"")) {
                        while (rs.next()) {

                            Patient_Doctor patient_Doctor = new Patient_Doctor();
                            patient_Doctor.setId_doctor(rs.getInt("id_doctor"));
                            patient_Doctor.setId_patient(rs.getInt("id_patient"));
                            patient_Doctors.add(patient_Doctor);
                        }
                    }
                } catch (SQLException ex) {
                    throw new DAOException("Impossible to get the list of users", ex);
                }
                
                try (PreparedStatement stm = CON.prepareStatement("INSERT INTO public.\"Exams\""
                        + "(id_patient, id_doctor, id_specialized_doctor, id_prescribable, date_exam, id_ticket)"
                        + "VALUES (?, ?, ?, ?, ?, ?);")) {
                    stm.setInt(1, patient_Doctors.get(r.nextInt(patient_Doctors.size())).id_patient);
                    stm.setInt(2, patient_Doctors.get(r.nextInt(patient_Doctors.size())).id_doctor);
                    stm.setInt(3, );
                    stm.setInt(4, id_prescribable);
                    stm.setDate(5, new java.sql.Date(date.getTime()));
                    stm.executeUpdate();
                } catch (SQLException ex) {
                    System.out.println("Errore");
                }
            } catch (DAOException ex) {
                Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Random x = new Random();
        int pat = 1;
        int med = 1;
        String s = "";
        int pre = 1;
        int presc = 1;
        boolean trovato = false;
        for (int i = 0; i < 10; i++) {
            try (PreparedStatement stm = CON.prepareStatement("SELECT MAX(id_prescription) AS t FROM \"public\".\"Prescriptions\"")) {
                try (ResultSet rs = stm.executeQuery()) {
                    rs.next();
                    pre = Integer.parseInt(rs.getString("t"));
                }
            } catch (SQLException ex) {
                System.out.println("Prescribable non trovato");
            }
            pre++;
            while (trovato == false) {
                try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM \"public\".\"Users\" WHERE id_user = ?")) {
                    pat = x.nextInt(63);
                    stm.setInt(1, pat);
                    try (ResultSet rs = stm.executeQuery()) {
                        rs.next();
                        s = rs.getString("role");
                        if (s.equals("paziente")) {
                            trovato = true;
                        }
                    }
                } catch (SQLException ex) {
                    System.out.println("Paziente non trovato");
                }
            }
            trovato = false;
            while (trovato == false) {
                try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM \"public\".\"Users\" WHERE id_user = ?")) {
                    med = x.nextInt(63);
                    stm.setInt(1, med);
                    try (ResultSet rs = stm.executeQuery()) {
                        rs.next();
                        s = rs.getString("role");
                        if (s.equals("MedicoBase")) {
                            trovato = true;
                        }
                    }
                } catch (SQLException ex) {
                    System.out.println("Base non trovato");
                }
            }
            int r = 100;
            try (PreparedStatement stm = CON.prepareStatement("SELECT MAX(id_ticket) AS t FROM \"public\".\"Tickets\"")) {
                try (ResultSet rs = stm.executeQuery()) {
                    rs.next();
                    r = Integer.parseInt(rs.getString("t"));
                   
                }
            } catch (SQLException ex) {
                System.out.println("Prescribable non trovato");
            }
            r++;
                       
            int d1_m = x.nextInt((12 - 1) + 1) + 1;
            int d2_m = x.nextInt((12 - 1) + 1) + 1;
            int d1_g = x.nextInt((31 - 1) + 1) + 1;
            int d2_g = x.nextInt((31 - 1) + 1) + 1;
            String d1 = "2019-" + d1_m + "-" + d1_g;
            String d2 = "2019-" + d2_m + "-" + d2_g;
            int value = x.nextInt((50 - 10) + 1) + 10;
            try (PreparedStatement stm3 = CON.prepareStatement("INSERT INTO \"public\".\"Tickets\" (id_ticket, value, paid, timestamp, exam) VALUES (?, ?, false, ? , false);")) {
                stm3.setInt(1, r);
                stm3.setInt(2, value);              
                stm3.setDate(3, java.sql.Date.valueOf(d1));
                stm3.executeUpdate();
            } catch (SQLException ex) {
                System.out.println("Errore Ticket");
            }
            
            try (PreparedStatement stm2 = CON.prepareStatement("INSERT INTO \"public\".\"Prescriptions\" (id_prescription, id_patient, id_doctor, description, date, id_pharmacy, id_ticket, date_erog) VALUES (?, ?, ?,?, ?, ?, ?, ?);")) {
                stm2.setInt(1, pre);
                stm2.setInt(2, pat);
                stm2.setInt(3, med);
                stm2.setString(4, null);
                stm2.setDate(5, java.sql.Date.valueOf(d1));
                stm2.setInt(6, 1);
                stm2.setInt(7, r);
                stm2.setDate(8, java.sql.Date.valueOf(d2));
                stm2.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);            
            }
            
            pat = 1;
            med = 1;
            s ="";
            pre = 1;
            presc = 1;
            trovato = false;
        }
    
    
         */

    }

    @Override
    public User getByPrimaryKey(Integer primaryKey) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
