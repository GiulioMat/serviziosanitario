/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.dao.jdbc;

import com.servizio.sanitario.persistence.dao.VisitDAO;
import com.servizio.sanitario.persistence.entities.Visit;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pretto Stefano
 */
public class JDBCVisitDAO extends JDBCDAO<Visit, Integer> implements VisitDAO {

    public JDBCVisitDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Visit getByPrimaryKey(Integer primaryKey) throws DAOException {
        Visit v = null;
        
        return v;
    }

    @Override
    public List<Visit> getAll() throws DAOException {
        List<Visit> visits = new ArrayList<>();
        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery("SELECT * FROM \"public\".\"Visits\"")) {
                while (rs.next()) {
                    Visit visit = new Visit();
                    visit.setId_visit(rs.getInt("id_visit"));
                    visit.setId_patient(rs.getInt("id_patient"));
                    visit.setId_doctor(rs.getInt("id_doctor"));
                    visit.setDate(rs.getDate("date"));
                    visits.add(visit);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }

        return visits;
    }

    @Override
    public List<Visit> getAllByPatient(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }
        List<Visit> visits = new ArrayList<>();
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM \"public\".\"Visits\" WHERE id_patient = ?")) {

            stm.setInt(1, primaryKey);
            try (ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Visit visit = new Visit();
                    visit.setId_visit(rs.getInt("id_visit"));
                    visit.setId_patient(rs.getInt("id_patient"));
                    visit.setId_doctor(rs.getInt("id_doctor"));
                    visit.setDate(rs.getDate("date"));
                    visits.add(visit);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }

        return visits;
    }
    @Override
    public List<Visit> getAllByDoctor(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }
        List<Visit> visits = new ArrayList<>();
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM \"public\".\"Visits\" WHERE id_doctor = ?")) {

            stm.setInt(1, primaryKey);
            try (ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Visit visit = new Visit();
                    visit.setId_visit(rs.getInt("id_visit"));
                    visit.setId_patient(rs.getInt("id_patient"));
                    visit.setId_doctor(rs.getInt("id_doctor"));
                    visit.setDate(rs.getDate("date"));
                    visits.add(visit);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }

        return visits;
    }

    @Override
    public void createnewVisit(int id_patient, int id_doctor, Date date) throws DAOException {
        System.out.println("com.servizio.sanitario.persistence.dao.jdbc.JDBCVisitDAO.createnewVisit()");
        try (PreparedStatement stm = CON.prepareStatement("INSERT INTO public.\"Visits\""
                + "(id_visit, id_patient, id_doctor, date)"
                + "VALUES (?, ?, ?, ?);")) {
            int id_visit = 0;
            try(PreparedStatement p2 = CON.prepareStatement("SELECT MAX(id_visit) AS t FROM \"public\".\"Visits\"")){
            try (ResultSet rs = p2.executeQuery()) {
                while (rs.next()) {
                    id_visit = rs.getInt("t");
                    System.out.println("id:"+id_visit);
                }
            }}catch (SQLException ex) {
                System.out.println("Id visita massimo non trovato");
            }
            id_visit++;
            stm.setInt(1, id_visit);
            stm.setInt(2, id_patient);
            stm.setInt(3, id_doctor);
            stm.setDate(4, date);
            stm.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

}
