/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.dao;


import com.servizio.sanitario.persistence.entities.Prescribable;
import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import com.servizio.sanitario.persistence.entities.User;
import java.util.List;

/**
 *
 * @author asus
 */
public interface PrescribableDAO extends DAO<Prescribable, Integer> {
    public List<Prescribable> getAll() throws DAOException;
}
