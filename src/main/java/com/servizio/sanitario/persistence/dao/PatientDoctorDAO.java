/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.dao;

import com.servizio.sanitario.persistence.entities.Patient_Doctor;
import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.util.List;

/**
 *
 * @author Pretto Stefano
 */
public interface PatientDoctorDAO extends DAO<Patient_Doctor, Integer>{
    public List<Patient_Doctor> getAll() throws DAOException;
    public Integer getDoctorId(int id_patient) throws DAOException;
    public void updateDoctorId(int newId_Patient, int newId_doctor) throws DAOException;
}
