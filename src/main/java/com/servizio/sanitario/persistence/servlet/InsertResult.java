/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.servlet;

import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import com.servizio.sanitario.persistence.dao.ExamDAO;
import com.servizio.sanitario.persistence.dao.TicketDAO;
import com.servizio.sanitario.persistence.dao.UserDAO;
import com.servizio.sanitario.persistence.entities.User;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import java.io.IOException;
import java.sql.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author andre
 */
public class InsertResult extends HttpServlet {

    private ExamDAO examDao;
    private TicketDAO ticketDao;
    private UserDAO userDao;

    public void init() throws ServletException {
        DAOFactory daoFactory;
        daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("to get dao factory for user storage system");
        }
        try {
            examDao = daoFactory.getDAO(ExamDAO.class);
            ticketDao = daoFactory.getDAO(TicketDAO.class);
            userDao = daoFactory.getDAO(UserDAO.class);

        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ticketDao.updatePaid(Integer.parseInt(request.getParameter("id_ticket")));
        response.sendRedirect("restricted/visits.jsp?type=vis&id_patient=" + request.getParameter("id_patient"));
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @return
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    //Form per l'inserimento del risultato di un esame con relativa notifica tramite e-mail dell'inserimento.
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String result = request.getParameter("result");
        int id_patient = Integer.parseInt(request.getParameter("id_patient"));
        int id_doctor = Integer.parseInt(request.getParameter("id_doctor"));
        int id_specialized = Integer.parseInt(request.getParameter("id_specialized"));
        Date date = Date.valueOf(request.getParameter("date"));
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            examDao.addResult(result, id_patient, id_doctor, id_specialized, date);
        } catch (DAOException ex) {
            System.out.println("Errore");
        }
        User user = null;
        try {
            user = userDao.getPrimary(id_patient);
        } catch (DAOException ex) {
            Logger.getLogger(InsertResult.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (user != null) {
            String to = user.getEmail();
            String from = "dmpserviziosanitario@gmail.com";
            ((HttpServletRequest) request).getSession().setAttribute("mailto", to);
            Properties props = new Properties();
            props.setProperty("mail.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.auth", "true");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

            Session session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(from, "xWU2yscMvXzL6zPh");
                }
            });
            response.setContentType("text/html");
            try {
                MimeMessage message = new MimeMessage(session);
                message.setFrom(new InternetAddress(from));
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
                message.setSubject("Nuovo risultato!");
                message.setText("Salve," + user.getFirstName() + ".\n La avvisiamo che un nuovo risultato per un suo esame è stato inserito. Per vedere l'esame clicchi sul seguente link: \n http://localhost:8080/Servizio-Sanitario/restricted/examsPatient.jsp");
                Transport.send(message);
                response.sendRedirect("restricted/visits.jsp?type=vis&id_patient=" + id_patient);
            } catch (AddressException ex) {
                Logger.getLogger(ForgotPasswordServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MessagingException ex) {
                Logger.getLogger(ForgotPasswordServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            response.sendRedirect("restricted/visits.jsp?type=vis&id_patient=" + id_patient);
        }
    }
}
