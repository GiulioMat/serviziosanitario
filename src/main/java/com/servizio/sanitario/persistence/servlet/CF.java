/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.servlet;

import com.servizio.sanitario.persistence.dao.UserDAO;
import com.servizio.sanitario.persistence.entities.User;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Pretto Stefano
 */
@WebServlet(name = "CF", urlPatterns = {"/CF"})
public class CF extends HttpServlet {

    String url = "http://webservices.dotnethell.it/codicefiscale.asmx/CalcolaCodiceFiscale";
    String charset = "UTF-8";  // Or in Java 7 and later, use the constant: java.nio.charset.StandardCharsets.UTF_8.name()
    String Nome = "STEFANO";
    String Cognome = "PRETTO";
    String ComuneNascita = "VICENZA";
    String DataNascita = "11/08/1998";
    String Sesso = "M";

    private UserDAO userDao;
    
    public void init() throws ServletException 
    { 
        DAOFactory daoFactory;
        daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("to get dao factory for user storage system");
        }
        try {
            userDao = daoFactory.getDAO(UserDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }  
    
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<User> users = new ArrayList<>();
        try {
            users = userDao.getAll();
        } catch (DAOException ex) {
            Logger.getLogger(CF.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        for(int i=0; i<users.size(); i++){
            if(!users.get(i).role.equals("SSP")){
                URLConnection connection = new URL(url).openConnection();
                connection.setDoOutput(true); // Triggers POST.
                connection.setRequestProperty("Accept-Charset", charset);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                String query = null;

                try {
                    Date date_of_birth = users.get(i).date_of_birth;
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
                    String strDate= formatter.format(date_of_birth); 

                    query = String.format("Nome=%s&Cognome=%s&ComuneNascita=%s&DataNascita=%s&Sesso=%s",
                        URLEncoder.encode(users.get(i).firstName, charset),
                        URLEncoder.encode(users.get(i).surname, charset),
                        URLEncoder.encode(users.get(i).province, charset),
                        URLEncoder.encode(strDate, charset),
                        URLEncoder.encode(users.get(i).sex, charset));
                } catch (UnsupportedEncodingException ex) {
                    System.out.println(ex);
                }

                try (OutputStream output = connection.getOutputStream()) {
                    output.write(query.getBytes(charset));
                }
                InputStream resp = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(resp));
                String cfString = "";
                String line;
                while((line = reader.readLine())!=null){
                    cfString += line+"\n";
                }
                resp.close();
                String cfFinale = cfString.split(">")[2].split("<")[0];
                System.out.println(cfFinale+"\n");
                try {
                    userDao.updateCF(cfFinale, users.get(i).id);
                } catch (DAOException ex) {
                    Logger.getLogger(CF.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        response.sendRedirect(request.getContextPath() + "/login.jsp");    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
