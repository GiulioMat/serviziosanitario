/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.servlet;

import com.servizio.sanitario.persistence.dao.UserDAO;
import com.servizio.sanitario.persistence.entities.User;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Servlet that handles the login web page.
 *
 * @author Stefano Chirico &lt;stefano dot chirico at unitn dot it&gt;
 * @since 2019.04.14
 */
public class LoginServlet extends HttpServlet {

    private UserDAO userDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory;
        daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("to get dao factory for user storage system");
        }
        try {
            userDao = daoFactory.getDAO(UserDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
    
    //Prende il token se esiste. In caso esista si estrae l'utente relativo a quel token dal db e rimanda alla pagina relativa al ruolo.
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String token_login = request.getParameter("token");
        User user = null;
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        try {
            user = userDao.getByToken(token_login);
            if (user == null) {
                response.sendRedirect(response.encodeRedirectURL(contextPath + "login.jsp"));
            }
            request.getSession().setAttribute("user", user);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "restricted/users.jsp"));
            } catch (DAOException ex) {
                Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
          
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     * dopost risponde ai metodi post 
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     *
     * @author Stefano Chirico
     * @since 1.0.0.190407
     */
    
    //Metodo per il login. Fa un controllo sui dati se corrispondono con i dati sul database. Infine controlla se la checkbox rememberpassword è selezionata ed in caso salva i dati sul token.
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String email = request.getParameter("username");
        String password = request.getParameter("password");
        String token_login = request.getParameter("token");
        
        String rememberMe= request.getParameter("rememberMe");
        String contextPath = getServletContext().getContextPath();
        
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        
        try {
            byte[] salt = userDao.getSalt(email);
            String psw_criptata = null;
            User user = null;
            if(salt != null && token_login == null){
                psw_criptata = get_SHA_256_SecurePassword(password, salt);
                user = userDao.getByEmailAndPassword(email, psw_criptata);
            }
            
            if (user == null) {
                response.sendRedirect(response.encodeRedirectURL(contextPath + "login.jsp"));
            } else {
                if(rememberMe!=null){  
                    String token = userDao.updateToken(email, psw_criptata);
                    Cookie tokenCookie = new Cookie("token", token);
                    tokenCookie.setMaxAge(24*60*60);
                    response.addCookie(tokenCookie);
                }
                
                request.getSession().setAttribute("user", user);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "restricted/users.jsp"));
            }
            
        } catch (DAOException ex) {
            //TODO: log exception
            request.getServletContext().log("Impossible to retrieve the user", ex);
        }
        
    }
    
    
    //Metodo per criptare la password
    private static String get_SHA_256_SecurePassword(String passwordToHash, byte[] salt)
    {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(salt);
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            return null;
        }
        
        return generatedPassword;
    }
    
}
