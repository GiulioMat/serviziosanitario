/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.servlet;

import com.servizio.sanitario.persistence.dao.UserDAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet("/photo")
@MultipartConfig(fileSizeThreshold = 1024*1024*2,
        maxFileSize = 1024 *1024*10,
        maxRequestSize = 1024 *1024 *50)

public class Photo extends HttpServlet {

    private UserDAO userDAO;
    
    public void init() throws ServletException { 
        DAOFactory daoFactory;
        daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("to get dao factory for user storage system");
        }
        try {
            userDAO = daoFactory.getDAO(UserDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, FileNotFoundException 
    {
        String userId = request.getParameter("userId");
        response.setContentType("text/html;charset=UTF-8");
        //Impostato cartella immagini
        Part part = request.getPart("file");
        String filename = getFileName(part);
        String savePath = "C:\\Users\\andre\\Desktop\\Servizio sanitario\\serviziosanitario\\src\\main\\webapp\\images" + File.separator + filename;
        part.write(savePath + File.separator);
        
        try {
            userDAO.updatePhoto(Integer.parseInt(userId), filename, savePath);
        } catch (DAOException ex) {
            Logger.getLogger(Photo.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException ex) {
            Logger.getLogger(Photo.class.getName()).log(Level.SEVERE, null, ex);
        }
        response.sendRedirect(request.getContextPath() + "/restricted/ProfileUser.jsp");
    }
        
    private String getFileName(final Part part) {
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                    content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }
    
    //Prende il path dal db e va ad estrarre la relativa immagine
    
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userId = req.getParameter("userId");
        int user = Integer.parseInt(userId);
        int i = 0;
        try {
            while(userDAO.getPathImgs(user,i)!=null){
                String photo = userDAO.getPathImgs(user,i);
                File file = new File(photo);
                file.delete();
                ++i;
            }
            userDAO.deletePhoto(user);
        } catch (Exception ex) {
            Logger.getLogger(Photo.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        resp.sendRedirect(req.getContextPath() + "/restricted/ProfileUser.jsp");
    }
   
}
