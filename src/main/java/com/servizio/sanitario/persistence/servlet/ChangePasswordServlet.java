/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.servlet;

import com.servizio.sanitario.persistence.dao.UserDAO;
import com.servizio.sanitario.persistence.entities.User;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author andre
 */
public class ChangePasswordServlet extends HttpServlet {
    private UserDAO userDao;
    
    @Override
    public void init() throws ServletException {        
        DAOFactory daoFactory;
        daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("to get dao factory for user storage system");
        }
        try {
            userDao = daoFactory.getDAO(UserDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     * dopost risponde ai metodi post 
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     *
     * @author 
     * @since 1.0.0.190407
     */
    
    //Servlet per cambio password dove controlla i due codici(mandato tramite mail ed inserito dall'utente).
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getParameter("userID");
        
        //Login>Password dimenticata
        if(userId==null){
            String code=request.getParameter("code");
            HttpSession session = ((HttpServletRequest) request).getSession();
            Object code2 = session.getAttribute("code");
            if(code.equals((String)code2))
            {
                ((HttpServletRequest) request).getSession().setAttribute("code", "");
                response.sendRedirect("enterNewPassword.jsp");
            }
            else{
                response.sendRedirect("insertCode.html");
            }
        }
        //Il Mio Account>Cambia Password
        else{
            User user = null;
            String psw = request.getParameter("password");
            String psw_inserita = null;
            
            try {
                user = userDao.getPrimary(Integer.parseInt(userId));
            } catch (DAOException ex) {
                Logger.getLogger(ChangePasswordServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            byte[] salt = userDao.getSalt(user.email);
            psw_inserita = get_SHA_256_SecurePassword(psw, salt);
            
            if(user.password.equals(psw_inserita)){
                response.sendRedirect(request.getContextPath() + "/enterNewPassword.jsp");
            }else{
                response.sendRedirect("ProfileUser.jsp");
            }
            
        }
   }
    
    
    //metodo per modificare la password
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String password=request.getParameter("password1");
        HttpSession session = ((HttpServletRequest) request).getSession();
        Object email = session.getAttribute("mailto");
        String psw_criptata = null;
        byte[] salt = new byte[16];
        
        try {
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            sr.nextBytes(salt);
            psw_criptata = get_SHA_256_SecurePassword(password, salt);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ChangePasswordServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(email==null){
            String emailProfile = request.getParameter("email");
            userDao.updatePsw(psw_criptata,salt,(String)emailProfile);
            response.sendRedirect(request.getContextPath() + "/restricted/ProfileUser.jsp");

        }else{
            userDao.updatePsw(psw_criptata,salt,(String)email);
            response.sendRedirect("login.jsp");
        }
        
        
    }   
    
    //metodo per criptatura password
    private static String get_SHA_256_SecurePassword(String passwordToHash, byte[] salt)
    {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(salt);
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            System.out.println("Errore");
            System.out.println(e);
        }
        
        return generatedPassword;
    }
}