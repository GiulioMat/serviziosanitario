/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.servlet;

import com.servizio.sanitario.persistence.dao.ExamDAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Pretto Stefano
 */
@WebServlet(name = "CreateExam", urlPatterns = {"/CreateExam"})
public class CreateExam extends HttpServlet {

    private ExamDAO examDao;
    
    public void init() throws ServletException 
    { 
        System.out.println("com.servizio.sanitario.persistence.servlet.Photo.init()");
        DAOFactory daoFactory;
        daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("to get dao factory for user storage system");
        }
        try {
            examDao = daoFactory.getDAO(ExamDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    //form per creazione esame
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
    {
        String patient_Id = request.getParameter("patientId");
        String examName = request.getParameter("examName");
        String doctorSpecializedId = request.getParameter("doctorSpecializedId");
        String doctorId = request.getParameter("doctorId");
        String date = request.getParameter("date");
        try {
            examDao.createnewExam(Integer.parseInt(patient_Id), Integer.parseInt(doctorId), Integer.parseInt(doctorSpecializedId),Integer.parseInt(examName), Date.valueOf(date));
        } catch (DAOException ex) {
            Logger.getLogger(CreateExam.class.getName()).log(Level.SEVERE, null, ex);
        }
        response.sendRedirect(request.getContextPath() + "/restricted/PrescriptionExam.jsp");
    }

    
}
