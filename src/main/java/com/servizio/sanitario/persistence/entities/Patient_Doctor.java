/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.entities;

/**
 *
 * @author Pretto Stefano
 */
public class Patient_Doctor {
    public Integer id_patient;
    public Integer id_doctor;

    public Patient_Doctor(Integer id_patient, Integer id_doctor) {
        this.id_patient = id_patient;
        this.id_doctor = id_doctor;
    }
    
    public Patient_Doctor(){}

    public Integer getId_patient() {
        return id_patient;
    }

    public Integer getId_doctor() {
        return id_doctor;
    }

    public void setId_patient(Integer id_patient) {
        this.id_patient = id_patient;
    }

    public void setId_doctor(Integer id_doctor) {
        this.id_doctor = id_doctor;
    }
    
    
}
