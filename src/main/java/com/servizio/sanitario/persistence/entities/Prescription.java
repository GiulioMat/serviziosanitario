/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.entities;

import java.sql.Date;

/**
 *
 * @author asus
 */
public class Prescription {
    public Integer id_prescription;
    public Integer id_patient;
    public Integer id_ticket;
    public Integer id_pharmacy;
    public Integer id_doctor;
    public Date date;
    public Date date_erog;
    public String description;
    

    public Prescription() {
    }

    public Prescription(Integer id_prescription, Integer id_patient, Integer id_doctor, Integer id_ticket, Integer id_pharmacy, Date date, Date date_erog, String description) {
        this.id_prescription = id_prescription;
        this.id_patient = id_patient;
        this.id_doctor = id_doctor;
        this.id_ticket = id_ticket;
        this.id_pharmacy = id_pharmacy;
        this.date = date;
        this.date_erog = date_erog;
        this.description = description;
    }

    public void setDate_erog(Date date_erog) {
        this.date_erog = date_erog;
    }

    public Date getDate_erog() {
        return date_erog;
    }

    public Integer getId_ticket() {
        return id_ticket;
    }

    public Integer getId_pharmacy() {
        return id_pharmacy;
    }
    
    public Integer getId_prescription() {
        return id_prescription;
    }

    public Integer getId_patient() {
        return id_patient;
    }

    public Integer getId_doctor() {
        return id_doctor;
    }

    public Date getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public void setId_prescription(Integer id_prescription) {
        this.id_prescription = id_prescription;
    }

    public void setId_patient(Integer id_patient) {
        this.id_patient = id_patient;
    }

    public void setId_doctor(Integer id_doctor) {
        this.id_doctor = id_doctor;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public void setId_ticket(Integer id_ticket) {
        this.id_ticket = id_ticket;
    }

    public void setId_pharmacy(Integer id_pharmacy) {
        this.id_pharmacy = id_pharmacy;
    }
    
}
