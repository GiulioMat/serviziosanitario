/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.entities;

import java.sql.Date;

/**
 *
 * @author Pretto Stefano
 */
public class User {
    public Integer id;
    public String email;
    public String password;
    public String firstName;
    public String surname;
    public String role;   
    public String cf;
    public String sex;
    public Date date_of_birth;
    public String VAT_number;
    public String province;
    public String token;
    public byte[] salt;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }
    
    
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
 
    
    public User(Integer id, String email, String password, String firstName, String lastName, String token) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.surname = lastName;
        this.token = token;
    }
    
    public User() {
        
    }

    public Integer getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public String getRole() {
        return role;
    }

    public String getCf() {
        return cf;
    }

    public Date getDate_of_birth() {
        return date_of_birth;
    }

    public String getVAT_number() {
        return VAT_number;
    }

    public String getProvince() {
        return province;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

   

    public void setRole(String role) {
        this.role = role;
    }

    public void setCf(String cf) {
        this.cf = cf;
    }

    public void setDate_of_birth(Date date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public void setVAT_number(String VAT_number) {
        this.VAT_number = VAT_number;
    }

    public void setProvince(String p_residence) {
        this.province = p_residence;
    }
    
    

}
