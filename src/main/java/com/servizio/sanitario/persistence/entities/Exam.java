/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.entities;

import java.sql.Date;

/**
 *
 * @author Pretto Stefano
 */
public class Exam {
    public Integer id_patient;
    public Integer id_doctor;
    public Integer id_specialized_doctor;
    public Integer id_ticket;
    public Integer id_prescribable;
    public Date date_exam;
    public String result; 
    public Boolean done;

    public Exam() {
    }

    public Exam(Integer id_patient, Integer id_doctor, Integer id_specialized_doctor, Integer id_ticket, Integer id_prescribable, Date date_exam, String result, Boolean done) {
        this.id_patient = id_patient;
        this.id_doctor = id_doctor;
        this.id_specialized_doctor = id_specialized_doctor;
        this.id_ticket = id_ticket;
        this.id_prescribable = id_prescribable;
        this.date_exam = date_exam;
        this.result = result;
        this.done = done;
    }

    public Integer getId_patient() {
        return id_patient;
    }

    public Integer getId_doctor() {
        return id_doctor;
    }

    public Integer getId_specialized_doctor() {
        return id_specialized_doctor;
    }

    public Integer getId_ticket() {
        return id_ticket;
    }

    public Integer getId_prescribable() {
        return id_prescribable;
    }

    public Date getDate_exam() {
        return date_exam;
    }

    public String getResult() {
        return result;
    }

    public Boolean getDone() {
        return done;
    }

    public void setId_patient(Integer id_patient) {
        this.id_patient = id_patient;
    }

    public void setId_doctor(Integer id_doctor) {
        this.id_doctor = id_doctor;
    }

    public void setId_specialized_doctor(Integer id_specialized_doctor) {
        this.id_specialized_doctor = id_specialized_doctor;
    }

    public void setId_ticket(Integer id_ticket) {
        this.id_ticket = id_ticket;
    }

    public void setId_prescribable(Integer id_prescribable) {
        this.id_prescribable = id_prescribable;
    }

    public void setDate_exam(Date date_exam) {
        this.date_exam = date_exam;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

}
