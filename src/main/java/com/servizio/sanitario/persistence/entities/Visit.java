/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.entities;

import java.sql.Date;

/**
 *
 * @author Pretto Stefano
 */
public class Visit {
    public Integer id_visit;
    public Integer id_patient;
    public Integer id_doctor;
    public Date date;
    
    public Visit() {
    }

    public Visit(Integer id_visit, Integer id_patient, Integer id_doctor, Date date) {
        this.id_visit = id_visit;
        this.id_patient = id_patient;
        this.id_doctor = id_doctor;
        this.date = date;
    }

    public Integer getId_visit() {
        return id_visit;
    }
    
    public Integer getId_patient() {
        return id_patient;
    }

    public Integer getId_doctor() {
        return id_doctor;
    }

    public Date getDate() {
        return date;
    }

    public void setId_visit(Integer id_visit) {
        this.id_visit = id_visit;
    }
    
    public void setId_patient(Integer id_patient) {
        this.id_patient = id_patient;
    }

    public void setId_doctor(Integer id_doctor) {
        this.id_doctor = id_doctor;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
