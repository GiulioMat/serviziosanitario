/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.entities;

/**
 *
 * @author asus
 */
public class Prescribable {
    public Integer id_prescribable;
    public String name;
    public String description;

    public Prescribable() {
    }

    public Prescribable(Integer id_prescribable, String name, String description) {
        this.id_prescribable = id_prescribable;
        this.name = name;
        this.description = description;
    }

    public Integer getId_prescribable() {
        return id_prescribable;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setId_prescribable(Integer id_prescribable) {
        this.id_prescribable = id_prescribable;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
    
    
}
