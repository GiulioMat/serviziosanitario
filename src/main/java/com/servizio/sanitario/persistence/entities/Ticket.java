/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servizio.sanitario.persistence.entities;

import java.sql.Date;

/**
 *
 * @author asus
 */
public class Ticket {
    public Integer id_ticket;
    public Integer value;
    public Boolean paid;
    public Boolean exam;
    public Date timestamp;

    public Ticket() {
    }

    public Ticket(Integer id_ticket, Integer value, Boolean paid, Boolean exam, Date timestamp) {
        this.id_ticket = id_ticket;
        this.value = value;
        this.paid = paid;
        this.exam = exam;
        this.timestamp = timestamp;
    }

    public void setExam(Boolean exam) {
        this.exam = exam;
    }

    public Boolean getExam() {
        return exam;
    }

    public Integer getId_ticket() {
        return id_ticket;
    }

    public Integer getValue() {
        return value;
    }

    public Boolean getPaid() {
        return paid;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setId_ticket(Integer id_ticket) {
        this.id_ticket = id_ticket;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
    
    
    
}
