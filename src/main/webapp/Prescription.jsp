<%-- 
Document   : PrescriptionExam
Created on : 23 ago 2019, 11:15:11
Author     : Pretto Stefano
--%>
<%@page import="com.servizio.sanitario.persistence.dao.UserDAO"%>
<%@page import="com.servizio.sanitario.persistence.dao.ExamDAO"%>
<%@page import="com.servizio.sanitario.persistence.dao.PatientDoctorDAO"%>
<%@page import="com.servizio.sanitario.persistence.entities.Exam"%>
<%@page import="com.servizio.sanitario.persistence.entities.Patient_Doctor"%>
<%@page import="com.servizio.sanitario.persistence.dao.PrescribableDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="https://code.jquery.com/jquery-3.3.1.js">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>

        <link href="${pageContext.request.contextPath}/customcss.css" rel="stylesheet">

        <title>Prescrivi ricetta</title>
    </head>

    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
            <div class="container">
                <c:if test="${sessionScope.user.role == 'MedicoBase'}">
                    <a class="navbar-brand" href="doctorhome.jsp">Servizio Sanitario</a>
                </c:if>
                <c:if test="${sessionScope.user.role == 'MedicoSpecialista'}">
                    <a class="navbar-brand" href="specializedhome.jsp">Servizio Sanitario</a>
                </c:if>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <c:if test="${sessionScope.user.role == 'MedicoBase'}">
                                <a class="nav-link" href="doctorhome.jsp">Home
                                    <span class="sr-only">(current)</span>
                                </a>
                            </c:if>
                            <c:if test="${sessionScope.user.role == 'MedicoSpecialista'}">
                                <a class="nav-link" href="specializedhome.jsp">Home
                                    <span class="sr-only">(current)</span>
                                </a>
                            </c:if>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<c:url value="../logout.handler"></c:url>">Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="form-gap"></div>

            <div class="container-fluid ">      
                <p class="font-weight-bold">Prescrivi una ricetta</p>
                <form action="${pageContext.request.contextPath}/createpresctiption.handler" method="post">
                <label class="pt-3" for="patientId">Seleziona paziente</label>  
                <c:set var="id" scope="session" value="${sessionScope.user.id}"/>
                <select class="custom-select" name="patientId" id="patientId">
                    <c:if test="${sessionScope.user.role == 'MedicoSpecialista'}">
                        <c:forEach var="patient" items="${userDao.all}">
                            <c:if test = "${patient.role =='paziente'}">
                                <option value="${patient.id}">${patient.firstName} ${patient.surname}</option>
                            </c:if>
                        </c:forEach> 
                    </c:if>
                    <c:if test="${sessionScope.user.role == 'MedicoBase'}">
                        <c:forEach var="utente" items="${userDao.all}">
                            <c:set var="doc" value="${PatientDoctorDao.getDoctorId(utente.id)}"/>
                            <c:if test="${(utente.role == 'paziente') && (doc==id)}">
                                <option value="${utente.id}">${utente.firstName} ${utente.surname}</option>
                            </c:if>
                        </c:forEach>
                    </c:if>          
                </select>


                <input type="hidden" name="doctorId" value="${sessionScope.user.id}">

                <label class="pt-4 w-100" for="description">Descrizione</label>  
                <textarea class="form-control" type="text" name="description" id="description" rows="3"></textarea>
                <input type="submit" name="Button" class="mt-3 btn btn-secondary active" value="Prescrivi">
            </form> 
        </div>

        <div class="pt-5"></div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>

        <script>
            $(".custom-select").select2({
                tags: "true",
                placeholder: "Seleziona un'opzione",
                theme: "classic",
                width: '100%',
                createTag: function (params) {
                    var term = $.trim(params.term);

                    if (term === '') {
                        return null;
                    }

                    return {
                        id: term,
                        text: term,
                        value: true // add additional parameters
                    };
                }
            });
        </script>
    </body>
</html>
