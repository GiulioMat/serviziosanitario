<%@page import="java.util.List"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException"%>
<%@page import="com.servizio.sanitario.persistence.entities.User"%>
<%@page import="com.servizio.sanitario.persistence.entities.Prescription"%>
<%@page import="com.servizio.sanitario.persistence.entities.Ticket"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory"%>
<%@page import="com.servizio.sanitario.persistence.dao.UserDAO"%>
<%@page import="com.servizio.sanitario.persistence.dao.PrescriptionDAO"%>
<%@page import="com.servizio.sanitario.persistence.dao.TicketDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:catch var="ex">
    <!DOCTYPE html>
    <html>

        <head>
            <!-- Required meta tags -->
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <!-- Bootstrap CSS -->

            <link rel="stylesheet" type="text/css" href="https://code.jquery.com/jquery-3.3.1.js">
            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">
            <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

            <link href="${pageContext.request.contextPath}/customcss.css" rel="stylesheet" >

            <title>Visite</title>
        </head>

        <body>
            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="myPatientSpecialized.jsp">Servizio Sanitario</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <c:if test="${sessionScope.user.role == 'MedicoBase'}">
                                    <a class="nav-link" href="doctorhome.jsp">Home
                                        <span class="sr-only">(current)</span>
                                    </a>
                                </c:if>
                                <c:if test="${sessionScope.user.role == 'MedicoSpecialista'}">
                                    <a class="nav-link" href="specializedhome.jsp">Home
                                        <span class="sr-only">(current)</span>
                                    </a>
                                </c:if>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<c:url value="../logout.handler"></c:url>">Logout</a>
                            </li>
                        </ul>
                    </div>
                    </div>
                </nav>
                <div class="form-gap"></div>
                <div class="container-fluid mb-5">
                    <table id="patientsTable" class="table table-striped table-bordered dt-responsive mb-5" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Paziente</th>
                                <th>Medico specializzato</th>
                                <th>Descrizione</th>
                                <th>Data</th>
                                <c:set var="url" value="${param.type}"/>
                            <th class="none">
                                Ticket
                            </th>
                            <th class="none">
                                <c:choose>
                                    <c:when test="${url == 'vis'}">
                                        Inserisci risultato
                                    </c:when>    
                                    <c:otherwise>
                                        Risultato
                                    </c:otherwise>
                                </c:choose>
                            </th>                                    
                        </tr>
                    </thead>
                    <tbody>
                        <c:set var="id_user" scope="session" value="${sessionScope.user.id}"/>
                        <c:set var="id" scope="session" value="${param.id_patient}"/>
                        
                        <c:forEach var="exam" items="${examDao.getAllByPrimaryKey(id)}">
                            <c:if test="${exam.id_specialized_doctor == id_user}">
                                <c:if test="${exam.done == false && url == 'vis'}">
                                    <tr>
                                        <c:set var="patient" value="${userDao.getPrimary(exam.id_patient)}"/>
                                        <td>${patient.firstName} ${patient.surname}</td>
                                        <c:set var="specialized" value="${userDao.getPrimary(exam.id_specialized_doctor)}"/>
                                        <td>${specialized.firstName} ${specialized.surname}</td>
                                        <c:set var="prescribable" value="${prescribableDao.getByPrimaryKey(exam.id_prescribable)}"/>
                                        <td>${prescribable.name}</td>
                                        <td>${exam.date_exam}</td>
                                        <th>
                                            <c:set var="ticket" value="${ticketDao.getByPrimaryKey(exam.id_ticket)}"/>
                                            ${ticket.value} &euro;
                                            <c:if test ="${ticket.paid == false}">
                                                <form class="form-signin" action="${pageContext.request.contextPath}/insertResult.handler" method="GET">
                                                    <input type="hidden" name="id_ticket" id="id_ticket" value="${ticket.id_ticket}">
                                                    <input type="hidden" name="id_patient" id="id_patient" value="${exam.id_patient}">
                                                    <input type="submit" value="Paga Ticket">  
                                                </form>
                                            </c:if>
                                        </th>
                                        <th>
                                            <form class="form-signin" action="${pageContext.request.contextPath}/insertResult.handler" method="POST">
                                                <input type="text" name="result" id="result" required="true">
                                                <input type="hidden" name="id_patient" id="id_patient" value="${exam.id_patient}">
                                                <input type="hidden" name="id_doctor" id="id_doctor" value="${exam.id_doctor}">
                                                <input type="hidden" name="id_specialized" id="id_specialized" value="${exam.id_specialized_doctor}">
                                                <input type="hidden" name="date" id="date" value="${exam.date_exam}">
                                                <input type="submit" value="Inserisci risultato esame">  
                                            </form> 
                                        </th>
                                    </tr>
                                </c:if>
                                <c:if test="${exam.done == true && url == 'storico'}">
                                    <tr>
                                        <c:set var="patient" value="${userDao.getPrimary(exam.id_patient)}"/>
                                        <td>${patient.firstName} ${patient.surname}</td>
                                        <c:set var="specialized" value="${userDao.getPrimary(exam.id_specialized_doctor)}"/>
                                        <td>${specialized.firstName} ${specialized.surname}</td>
                                        <c:set var="prescribable" value="${prescribableDao.getByPrimaryKey(exam.id_prescribable)}"/>
                                        <td>${prescribable.name}</td>
                                        <td>${exam.date_exam}</td>
                                        <th>
                                            <c:set var="ticket" value="${ticketDao.getByPrimaryKey(exam.id_ticket)}"/>
                                            ${ticket.value} &euro;
                                            <c:if test ="${ticket.paid == false}">
                                                <form class="form-signin" action="${pageContext.request.contextPath}/insertResult.handler" method="GET">
                                                    <input type="hidden" name="id_ticket" id="id_ticket" value="${ticket.id_ticket}">
                                                    <input type="hidden" name="id_patient" id="id_patient" value="${exam.id_patient}">
                                                    <input type="submit" value="Paga Ticket">  
                                                </form>
                                            </c:if>
                                        </th>
                                        <th>
                                            ${exam.result}
                                        </th>
                                    </tr>
                                </c:if> 
                            </c:if>
                        </c:forEach>                             
                    </tbody>  
                    <tfoot>
                        <tr>
                            <th>Paziente</th>
                            <th>Medico specializzato</th>
                            <th>Descrizione</th>
                            <th>Data</th>
                                <c:set var="url" value="${param.type}"/>
                            <th class="none">
                                Ticket
                            </th>
                            <th class="none">
                                <c:choose>
                                    <c:when test="${url == 'vis'}">
                                        Inserisci risultato
                                    </c:when>    
                                    <c:otherwise>
                                        Risultato
                                    </c:otherwise>
                                </c:choose>
                            </th>                                    
                        </tr>
                    </tfoot>
                </table>
            </div>

            <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap4.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
            <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        </body>
    </html>
    <script>

        $(document).ready(function () {
            var table = $('#patientsTable').DataTable({
                responsive: true,
                info: false
            });
        });


    </script>
</c:catch>
<c:if test="${not empty ex}">
    <jsp:scriptlet>
        response.sendError(500, ((Exception) pageContext.getAttribute("ex")).getMessage());
    </jsp:scriptlet>
</c:if>
