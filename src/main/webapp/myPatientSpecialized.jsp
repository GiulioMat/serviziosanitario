<%@page import="java.util.List"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException"%>
<%@page import="com.servizio.sanitario.persistence.entities.User"%>
<%@page import="com.servizio.sanitario.persistence.entities.Prescription"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory"%>
<%@page import="com.servizio.sanitario.persistence.dao.UserDAO"%>
<%@page import="com.servizio.sanitario.persistence.dao.PrescriptionDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:catch var="ex">
    <!DOCTYPE html>
    <html>

        <head>
            <!-- Required meta tags -->
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <!-- Bootstrap CSS -->

            <link rel="stylesheet" type="text/css" href="https://code.jquery.com/jquery-3.3.1.js">
            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">
            <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

            <link href="${pageContext.request.contextPath}/customcss.css" rel="stylesheet" >

            <title>Lista Pazienti</title>
        </head>

        <body>
            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="specializedhome.jsp">Servizio Sanitario</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="specializedhome.jsp">Home
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<c:url value="../logout.handler"></c:url>">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div class="form-gap"></div>

                <div class="container-fluid mb-5">
                    <table id="patients" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Paziente</th>
                                <th class="none">Profilo Paziente</th>
                                <th class="none">Esami da fare</th>
                                <th class="none">Storico esami</th>

                            </tr>
                        </thead>
                        <tbody>
                        <c:set var="id" scope="session" value="${sessionScope.user.id}"/>
                        <%--user rappresenta tutti i pazienti di quel medico--%>
                        <c:forEach var="user" items="${userDao.all}">       
                            <c:if test="${user.role == 'paziente'}">
                                <tr>
                                    <td>${user.firstName} ${user.surname}</td>
                                    <td><a href="./ProfileUser.jsp?type=profile&id_patient=<c:out value="${user.id}"/>">Profilo Paziente</a></td>                                    
                                    <td><a href="./visits.jsp?type=vis&id_patient=<c:out value="${user.id}"/>">Esami da fare</a></td>
                                    <td><a href="./visits.jsp?type=storico&id_patient=<c:out value="${user.id}"/>">Storico esami</a></td>
                                </tr> 
                            </c:if>
                        </c:forEach>                             
                    </tbody>  
                    <tfoot>
                        <tr>
                            <th>Paziente</th>
                            <th>Profilo Paziente</th>
                            <th>Esami da fare</th>
                            <th>Storico esami</th>
                        </tr>
                    </tfoot>
                </table>
            </div>


            <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap4.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
            <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.js"></script>
        </body>


    </html>
    <script type="text/javascript">
        $(document).ready(function () {
            var dataSrc = [];

            var table = $('#patients').DataTable({
                responsive: true,
                info: false,

                'initComplete': function () {
                    var api = this.api();

                    // Populate a dataset for autocomplete functionality
                    // using data from first, second and third columns
                    api.cells('tr', [0]).every(function () {
                        // Get cell data as plain text
                        var data = $('<div>').html(this.data()).text();
                        if (dataSrc.indexOf(data) === -1) {
                            dataSrc.push(data);
                        }
                    });

                    // Sort dataset alphabetically
                    dataSrc.sort();

                    // Initialize Typeahead plug-in
                    $('.dataTables_filter input[type="search"]', api.table().container())
                            .typeahead({
                                source: dataSrc,
                                afterSelect: function (value) {
                                    api.search(value).draw();
                                }
                            }
                            );
                }
            });
        });
    </script>
</c:catch>
<c:if test="${not empty ex}">
    <jsp:scriptlet>
        response.sendError(500, ((Exception) pageContext.getAttribute("ex")).getMessage());
    </jsp:scriptlet>
</c:if>