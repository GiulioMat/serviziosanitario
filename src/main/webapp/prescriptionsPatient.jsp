

<%@page import="java.util.List"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException"%>
<%@page import="com.servizio.sanitario.persistence.entities.User"%>
<%@page import="com.servizio.sanitario.persistence.entities.Prescription"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory"%>
<%@page import="com.servizio.sanitario.persistence.dao.UserDAO"%>
<%@page import="com.servizio.sanitario.persistence.dao.PrescriptionDAO"%>
<%@page import="java.net.URLEncoder"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:catch var="ex">
    <!DOCTYPE html>
    <html>
        <head>
            <!-- Required meta tags -->
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <!-- Bootstrap CSS -->

            <link rel="stylesheet" type="text/css" href="https://code.jquery.com/jquery-3.3.1.js">
            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap4.min.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.1/css/select.bootstrap4.min.css">
            <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

            <link href="${pageContext.request.contextPath}/customcss.css" rel="stylesheet">

            <title>Ricette</title>
        </head>

        <body>
            <!-- Navigation -->
            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                <div class="container">
                    <c:set var="a" value="${pageContext.request.getParameter('id_patient')}"/>
                    <c:choose>
                        <c:when test="${a == null}">
                            <a class="navbar-brand" href="patienthome.jsp">Servizio Sanitario</a>
                        </c:when>    
                        <c:otherwise>
                            <a class="navbar-brand" href="myPatientDoctor.jsp">Servizio Sanitario</a>
                        </c:otherwise>
                    </c:choose>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <c:if test="${a == null}">
                                    <a class="nav-link" href="patienthome.jsp">Home
                                        <span class="sr-only">(current)</span>
                                    </a>
                                </c:if>    
                                <c:if test="${a != null}">
                                    <a class="nav-link" href="doctorhome.jsp">Home
                                        <span class="sr-only">(current)</span>
                                    </a>
                                </c:if>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<c:url value="../logout.handler"></c:url>">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div class="form-gap"></div>

                <div class="container-fluid">
                    <!-- Tabella -->
                    <table id="prescriptionsTable" class="table table-striped table-bordered dt-responsive nowrap mb-5" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Codice Ricetta</th>
                                <th>Descrizione</th>
                                <th>Data</th>
                            </tr>
                        </thead>
                        <tbody>
                        <c:choose>
                            <c:when test="${pageContext.request.queryString == null}">
                                <c:set var="id_user" scope="session" value="${sessionScope.user.id}"/>
                            </c:when>    
                            <c:otherwise>
                                <c:set var="id_user" value="${param.id_patient}"/>
                            </c:otherwise>
                        </c:choose>


                        <c:forEach var="prescription" items="${prescriptionDao.all}"> 

                            <c:if test="${prescription.id_patient == id_user}">
                                <tr>
                                    <td>${prescription.id_prescription}</td>
                                    <td>${prescription.description}</td>
                                    <td>${prescription.date}</td>                                        
                                </tr> 
                            </c:if>
                        </c:forEach>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Codice Ricetta</th>
                            <th>Descrizione</th>
                            <th>Data</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap4.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

            <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>

            <c:set var="doctor" value="${PatientDoctorDao.getDoctorId(user.id)}"/>


            <c:choose>
                <c:when test="${a == null}">
                    <script type="text/javascript">
                        var url;
                        $(document).ready(function () {
                            var table = $('#prescriptionsTable').DataTable({
                                dom: "<'row'<'col-sm-12 col-md-4'l><'col-sm-12 col-md-4 text-center'B><'col-sm-12 col-md-4'f>>" + "trp",
                                buttons: [{
                                        extend: 'print',
                                        text: 'Stampa Ricetta Selezionata',
                                        action: function (e, dt, button, config) {
                                            var data = $('#prescriptionsTable').DataTable().row('.selected').data();
                                            var QRData = "Codice Prescrizione: " + data[0] + "; Codice Medico: ${doctor}; Codice Fiscale: ${user.cf}; Descrizione: " + data[1] + "; Data: " + data[2];
                                            ;
                                            url = "https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=" + encodeURIComponent(QRData) + "&choe=UTF-8";
                                            $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
                                        },
                                        customize: function (win) {
                                            $(win.document.body)
                                                    .css('font-size', '10pt')
                                                    .prepend(
                                                            '<img src="' + url + '" style="position:absolute; left:50%; top:50%; margin-left: -150px; margin-top: -150px" />'
                                                            );
                                            $(win.document.body).find('table')
                                                    .addClass('compact')
                                                    .css('font-size', 'inherit');
                                        }
                                    }],
                                select: true,
                                responsive: true
                            });
                        });
                    </script>
                </c:when>    
                <c:otherwise>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            var table = $('#prescriptionsTable').DataTable({
                                responsive: true,
                                info: false
                            });
                        });
                    </script>
                </c:otherwise>
            </c:choose>




        </body>
    </html>

</c:catch>
<c:if test="${not empty ex}">
    <jsp:scriptlet>
        response.sendError(500, ((Exception) pageContext.getAttribute("ex")).getMessage());
    </jsp:scriptlet>
</c:if>