<%-- 
    Document   : NewPassword
    Created on : 25 set 2019, 10:05:03
    Author     : Pretto Stefano
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Inseriscri la nuova password</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!-- Custom styles for this template -->
        <link href="css/floating-labels.css" rel="stylesheet">
    </head>
    <body>
        <p align ="center">
        <form class="form-signin" action="changePassword.handler" method="GET">
            <div class="text-center mb-4">
                <img src="images/logoz.jpg">
                <p>Inserisci la nuovo Password</p>

            </div>
            <div>
                Nuova Password:
                <input type="text" id="password" name="password" class="form-control" required autofocus>
            </div><br/>
            <button class="btn btn-lg btn-primary btn-block btn-dark" type="submit">Resetta</button>
        </form>
    </body>
</html>
