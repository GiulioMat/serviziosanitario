<%-- 
    Document   : patienthome
    Created on : 1 nov 2019, 18:01:39
    Author     : asus
--%>

<%@page import="java.util.List"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException"%>
<%@page import="com.servizio.sanitario.persistence.entities.User"%>
<%@page import="com.servizio.sanitario.persistence.entities.Prescribable"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory"%>
<%@page import="com.servizio.sanitario.persistence.dao.UserDAO"%>
<%@page import="com.servizio.sanitario.persistence.dao.PrescribableDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<c:catch var="ex">
    <!DOCTYPE html>
    <html>
        <head>
            <!-- Required meta tags -->
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <!-- Bootstrap CSS -->
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

            <link href="${pageContext.request.contextPath}/customcss.css" rel="stylesheet" > 
            <title>Servizio Sanitario</title>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
            <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
        </head>
        <body>
            <!-- Navigation -->
            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="patienthome.jsp">Servizio Sanitario</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="patienthome.jsp">Home
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <c:if test="${sessionScope.user.role == 'MedicoBase' || sessionScope.user.role == 'MedicoSpecialista'}">
                                <li class="nav-item">
                                    <a class="nav-link" href="users.jsp">Utente</a>
                                </li>
                            </c:if>
                            <li class="nav-item">
                                <a class="nav-link" href="<c:url value="../logout.handler"></c:url>">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

            <c:set var ="name_exams" value="${prescribableDao.all}"/>
            <c:set var="length" value="${fn:length(name_exams)}"/>   


            <div class="form-gap"></div>

            <div class="cards-list">

                <div class="card 1">
                    <a href="ProfileUser.jsp" class="card_image"> 
                        <img src="https://source.unsplash.com/QnUywvDdI1o" /> 
                    </a>
                    <div class="card_title title-white">
                        <p>Il mio account</p>
                    </div>
                </div>

                <div class="card 2">
                    <a href="prescriptionsPatient.jsp" class="card_image">
                        <img src="https://source.unsplash.com/Lus2rqA3jz4" />
                    </a>
                    <div class="card_title title-white">
                        <p>Ricette</p>
                    </div>
                </div>

                <div class="card 3">
                    <a href="examsPatient.jsp" class="card_image">
                        <img src="https://source.unsplash.com/rD2dc_2S3i0" />
                    </a>
                    <div class="card_title title-white">
                        <p>Esami</p>
                    </div>
                </div>

                <div class="card 4">
                    <a href="ticketsPatient.jsp" class="card_image">
                        <img src="https://source.unsplash.com/UX-mCYFC1cA" />
                    </a>
                    <div class="card_title title-white">
                        <p>Ticket</p>
                    </div>
                </div>
                
                <div class="card 5">
                    <a href="visits2.jsp" class="card_image">
                        <img src="https://source.unsplash.com/8u_2imJaVQs" />
                    </a>
                    <div class="card_title title-white">
                        <p>Visite</p>
                    </div>
                </div>
                
            </div>

            <div class="pt-5"></div>

            <div class="container-fluid pb-5">
                <div class="row">
                    <div class="col-12 col-sm-6 mx-auto text-center">
                        <c:set var="url" value="${pageContext.request.requestURI }" />  
                        <c:set var="exam" value="${pageContext.request.getParameter('examName')}"/>
                        <c:choose>
                            <c:when test="${exam == null}">
                                <p class="font-weight-bold">Ricerca Esame</p>
                                <form action="${url}" method="post">
                                    <div>
                                        <select name="examName" class="custom-select">
                                            <c:forEach var = "i" begin = "0" end = "${length}">
                                                <option value="${name_exams[i].description}">${name_exams[i].name}</option>
                                            </c:forEach> 
                                        </select>
                                    </div>
                                    <input class="mt-3 btn btn-secondary active" type="submit" name="Button" value="Dettagli Esami">
                                </form>
                            </c:when>
                            <c:otherwise>
                                <p class="font-weight-bold">Descrizione Esame</p>
                                ${exam}
                                <form action="${url}" method="POST">
                                    <input class="mt-3 btn btn-secondary active" type="submit" name="Button" value="Ricerca Esami">
                                </form>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>     


            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
            <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

            <script>
                $(".custom-select").select2({
                    tags: "true",
                    placeholder: "Seleziona un'opzione",
                    theme: "classic",
                    width: '100%',
                    createTag: function (params) {
                        var term = $.trim(params.term);

                        if (term === '') {
                            return null;
                        }

                        return {
                            id: term,
                            text: term,
                            value: true // add additional parameters
                        };
                    }
                });
            </script>
        </body>
    </html>
</c:catch>
<c:if test="${not empty ex}">
    <jsp:scriptlet>
        response.sendError(500, ((Exception) pageContext.getAttribute("ex")).getMessage());
    </jsp:scriptlet>
</c:if>

