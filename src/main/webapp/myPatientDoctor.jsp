<%@page import="java.util.List"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException"%>
<%@page import="com.servizio.sanitario.persistence.entities.User"%>
<%@page import="com.servizio.sanitario.persistence.entities.Prescription"%>
<%@page import="com.servizio.sanitario.persistence.entities.Patient_Doctor"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory"%>
<%@page import="com.servizio.sanitario.persistence.dao.UserDAO"%>
<%@page import="com.servizio.sanitario.persistence.dao.PatientDoctorDAO"%>
<%@page import="com.servizio.sanitario.persistence.dao.PrescriptionDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:catch var="ex">
    <!DOCTYPE html>
    <html>

        <head>
            <!-- Required meta tags -->
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

            <title>Lista pazienti</title>

            <link rel="stylesheet" type="text/css" href="https://code.jquery.com/jquery-3.3.1.js">
            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">
            <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

            <link href="${pageContext.request.contextPath}/customcss.css" rel="stylesheet" >
        </head>

        <body>
            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="doctorhome.jsp">Servizio Sanitario</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="doctorhome.jsp">Home
                                    <span class="sr-only">(current)</span>
                                </a> 
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<c:url value="../logout.handler"></c:url>">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <!-- Table -->

                <div class="form-gap"></div>

                <div class="container-fluid">
                    <!-- Table -->
                    <table id="patientsTable" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Paziente</th>
                                <th>Nome ultimo esame</th>
                                <th>Data ultimo esame</th>
                                <th>Risultati ultimo esame</th>
                                <th>Data ultima prescrizione</th>
                                <th>Descrizione ultima prescrizione</th>
                                <th class="none">Tutte le prescrizioni</th>
                                <th class="none">Tutti gli esami</th>
                                <th class="none">Assegna un esame</th>

                            </tr>
                        </thead>
                        <tbody>

                        <c:set var="id" scope="session" value="${sessionScope.user.id}"/>
                        <c:forEach var="utente" items="${userDao.all}">
                            <c:set var="doc" value="${PatientDoctorDao.getDoctorId(utente.id)}"/>
                            <c:if test="${doc != null}">
                                <c:if test="${(utente.role == 'paziente') && (doc==id)}">
                                    <tr>
                                        <td>${utente.firstName} ${utente.surname}</td>
                                        <c:set var="patient" value="${utente.id}"/>
                                        <c:set var="booking" value="${examDao.getLastExam(patient)}"/>
                                        <c:if test="${booking!=null}">
                                            <c:set var="prescribable" value="${prescribableDao.getByPrimaryKey(booking.id_prescribable)}"/>
                                            <td>${prescribable.name}</td>
                                            <td>${booking.date_exam}</td>
                                            <c:if test="${booking.result != null}">
                                                <td>${booking.result}</td>
                                            </c:if>
                                            <c:if test="${booking.result == null}">
                                                <td>Risultati non disponibili</td>
                                            </c:if>
                                        </c:if>
                                        <c:if test="${booking==null}">
                                            <td>Nessuna visita</td>
                                            <td>Nessuna visita</td>
                                            <td>Nessuna visita</td>
                                        </c:if>     
                                        <c:set var="prescription" value="${prescriptionDao.getLastPrescription(patient)}"/>
                                        <c:if test="${prescription!=null}">
                                            <td>${prescription.date}</td>
                                            <td>${prescription.description}</td>
                                            <td><a href="./prescriptionsPatient.jsp?id_patient=<c:out value="${patient}"/>">Tutte le prescrizioni</a></td>
                                        </c:if>
                                        <c:if test="${prescription==null}">
                                            <td>Nessuna prescrizione</td>
                                            <td>Nessuna prescrizione</td>
                                            <td>Nessuna prescrizione</td>
                                        </c:if>  
                                        <c:if test="${booking!=null}">
                                            <td><a href="./examsPatient.jsp?id_patient=<c:out value="${patient}"/>">Tutti gli esami</a></td>
                                        </c:if>
                                        <c:if test="${booking==null}">
                                            <td>Nessun esame</td>
                                        </c:if>
                                        <td><a href="./PrescriptionExam.jsp?id_patient=<c:out value="${patient}"/>">Assegna un esame</a></td>
                                    </tr> 
                                </c:if>
                            </c:if>
                        </c:forEach>  
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Paziente</th>
                            <th>Nome ultimo esame</th>
                            <th>Data ultimo esame</th>
                            <th>Risultati ultimo esame</th>
                            <th>Data ultima prescrizione</th>
                            <th>Descrizione ultima prescrizione</th>
                            <th>Tutte le prescrizioni</th>
                            <th>Tutti gli esami</th>
                            <th>Assegna un esame</th>

                        </tr>
                    </tfoot>

                </table>
            </div>

            <div class=" mb-5"></div>


            <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap4.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
            <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.js"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    var dataSrc = [];

                    var table = $('#patientsTable').DataTable({
                        responsive: true,
                        info: false,

                        'initComplete': function () {
                            var api = this.api();

                            // Populate a dataset for autocomplete functionality
                            // using data from first, second and third columns
                            api.cells('tr', [0]).every(function () {
                                // Get cell data as plain text
                                var data = $('<div>').html(this.data()).text();
                                if (dataSrc.indexOf(data) === -1) {
                                    dataSrc.push(data);
                                }
                            });

                            // Sort dataset alphabetically
                            dataSrc.sort();

                            // Initialize Typeahead plug-in
                            $('.dataTables_filter input[type="search"]', api.table().container())
                                    .typeahead({
                                        source: dataSrc,
                                        afterSelect: function (value) {
                                            api.search(value).draw();
                                        }
                                    }
                                    );
                        }
                    });
                });
            </script>
        </body>
    </html>

</c:catch>
<c:if test="${not empty ex}">
    <jsp:scriptlet>
        response.sendError(500, ((Exception) pageContext.getAttribute("ex")).getMessage());
    </jsp:scriptlet>
</c:if>