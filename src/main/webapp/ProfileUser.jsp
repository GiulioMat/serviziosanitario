<%-- 
Document   : ProfileUser
Created on : 12 ago 2019, 15:15:09
Author     : Pretto Stefano
--%>

<%@page import="java.util.List"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException"%>
<%@page import="com.servizio.sanitario.persistence.entities.User"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory"%>
<%@page import="com.servizio.sanitario.persistence.dao.UserDAO"%>
<%@page import="com.servizio.sanitario.persistence.dao.PatientDoctorDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

        <link href="${pageContext.request.contextPath}/customcss.css" rel="stylesheet">

        <title>Profilo</title>
    </head>

    <body>

        <c:set var="user_id" value="${sessionScope.user.id}"/>
        <c:set var="user_photo" value="${userDao.getPrimary(user_id)}"/>
        <c:set var="url" value="${pageContext.request.getParameter('type')}" />
        <c:if test="${url != 'profile' }">
            <!-- Navigation -->
            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="patienthome.jsp">Servizio Sanitario</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="patienthome.jsp">Home
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                    <a class="nav-link" href="<c:url value="../logout.handler"></c:url>">Logout</a>                               
                            </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div class="form-gap"></div>

                <div class="container-fluid">
                <c:if test='${userDao.getFileName(0,user_id) == null}'>
                    <div class="row">
                        <div class="col-8 col-sm-4 mx-auto text-center">
                            <img src="<c:url value="/images/avatars/profile.jpg"/>" height="150px" width="150px"/>
                        </div>
                    </div>
                </c:if>        
                <c:if test='${userDao.getFileName(0,user_id)!= null}'>
                    <div class="row">
                        <div class="col-8 col-sm-4 mx-auto text-center">
                            <img src="<c:url value="/images/${userDao.getFileName(0,user_id)}"/>" height="150px" width="150px"/>
                        </div>
                    </div>
                </c:if>

                <div class="row pt-5">
                    <form class="col-12 col-md-8 col-lg-6 col-xl-4" action="${pageContext.request.contextPath}/photo.handler" method="post" enctype="multipart/form-data">
                        <input class="mt-2" name="file" type="file" id="file" multiple>
                        <input class="mt-2" type="hidden" name="userId" value="${sessionScope.user.id}">
                        <input class="mt-2" name="submit" type="submit" value="Conferma">
                    </form>
                    <form class="col-6 col-md-2" action="${pageContext.request.contextPath}/photo.handler" method="get">
                        <input class="mt-2" type="hidden" name="userId" value="${sessionScope.user.id}">
                        <input class="mt-2" name="submit_2" type="submit" value="Cancella Foto">
                    </form>
                </div>   

                <c:set var="id_doctor" value="${PatientDoctorDao.getDoctorId(sessionScope.user.id)}"/>
                <c:set var="user1" value="${userDao.getPrimary(id_doctor)}"/>

                <div class="row pt-5">
                    <ul class="col list-group list-group-flush">
                        <li class="list-group-item">Nome: ${sessionScope.user.firstName}</li>
                        <li class="list-group-item">Cognome: ${sessionScope.user.surname}</li>
                        <li class="list-group-item">Data di nascita: ${sessionScope.user.date_of_birth}</li>
                        <li class="list-group-item">Codice fiscale: ${sessionScope.user.cf}</li>
                        <li class="list-group-item">Provincia: ${sessionScope.user.province}</li>
                        <li class="list-group-item">Indirizzo e-mail: ${sessionScope.user.email}</li>
                        <li class="list-group-item">Medico di base: ${user1.firstName} ${user1.surname}</li>
                    </ul>
                </div>   
                    
                  
                <form action="changePassword.handler" method="POST" >
                    <label class="font-weight-bold pt-5" for="password">Vuoi cambiare la password?</label>
                    <input type="password" id="password" name="password" class="form-control" placeholder="Password attuale">
                    <input class="mt-2" type="hidden" id="userID" name="userID" value="${sessionScope.user.id}">
                    <input class="mt-2" type="submit" name="Button" value="Cambia">
                </form> 

                <form action="${pageContext.request.contextPath}/ChangeDoctor.handler" method="get">
                    <label class="font-weight-bold pt-5" for="doctorID">Vuoi cambiare il medico di base?</label>
                    <select class="form-control" name="doctorID" id="doctorID">
                        <c:forEach var="doctor" items="${userDao.getAllforDoctor(sessionScope.user.id)}">                      
                            <c:if test = "${doctor.role =='MedicoBase' && doctor.province == sessionScope.user.province && doctor.id != user1.id}">
                                <option value="${doctor.id}">${doctor.firstName} ${doctor.surname}</option>
                            </c:if>
                        </c:forEach> 
                    </select>
                    <input class="mt-2" type="hidden" name="userID" value="${sessionScope.user.id}">
                    <input class="mt-2" type="submit" name="Button" value="Cambia">
                </form> 
                <div class="pt-5"></div>
            </div>
        </c:if>
        <c:if test="${url == 'profile' }">
            <c:set var="id" value="${pageContext.request.getParameter('id_patient')}" />
            <!-- Navigation -->
            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="myPatientSpecialized.jsp">Servizio Sanitario</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <c:if test="${sessionScope.user.role == 'MedicoBase'}">
                                    <a class="nav-link" href="doctorhome.jsp">Home
                                        <span class="sr-only">(current)</span>
                                    </a>
                                </c:if>
                                <c:if test="${sessionScope.user.role == 'MedicoSpecialista'}">
                                    <a class="nav-link" href="specializedhome.jsp">Home
                                        <span class="sr-only">(current)</span>
                                    </a>
                                </c:if>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<c:url value="../logout.handler"></c:url>">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div class="form-gap"></div>

                <div class="container-fluid mb-5">
                <c:if test='${userDao.getFileName(0,id) == null}'>
                    <div class="row">
                        <div class="col-8 col-sm-4 mx-auto text-center">
                            <img src="<c:url value="/images/avatars/profile.jpg"/>" height="150px" width="150px"/>
                        </div>
                    </div>
                </c:if>        
                <c:if test='${userDao.getFileName(0,id)!= null}'>
                    <div class="row">
                        <div class="col-8 col-sm-4 mx-auto text-center">
                            <img src="<c:url value="/images/${userDao.getFileName(0,id)}"/>" height="150px" width="150px"/>
                        </div>
                    </div>
                </c:if>

                <c:set var="id_doctor" value="${PatientDoctorDao.getDoctorId(id)}"/>
                <c:set var="user1" value="${userDao.getPrimary(id_doctor)}"/>
                <c:set var="paziente" value="${userDao.getPrimary(id)}"/>

                <div class="row pt-5">
                    <ul class="col list-group list-group-flush">
                        <li class="list-group-item">Nome: ${paziente.firstName}</li>
                        <li class="list-group-item">Cognome: ${paziente.surname}</li>
                        <li class="list-group-item">Data di nascita: ${paziente.date_of_birth}</li>
                        <li class="list-group-item">Codice fiscale: ${paziente.cf}</li>
                        <li class="list-group-item">Provincia: ${paziente.province}</li>
                        <li class="list-group-item">Indirizzo e-mail: ${paziente.email}</li>
                        <li class="list-group-item">Medico di base: ${user1.firstName} ${user1.surname}</li>
                    </ul>
                </div>        
            </div>
        </c:if>

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>

    </body>
</html>
