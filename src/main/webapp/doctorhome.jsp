<%-- 
    Document   : doctorhome
    Created on : 1 nov 2019, 18:01:48
    Author     : asus
--%>

<%@page import="java.util.List"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException"%>
<%@page import="com.servizio.sanitario.persistence.entities.User"%>
<%@page import="com.servizio.sanitario.persistence.entities.Prescribable"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory"%>
<%@page import="com.servizio.sanitario.persistence.dao.UserDAO"%>
<%@page import="com.servizio.sanitario.persistence.dao.PrescribableDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<c:catch var="ex">
    <!DOCTYPE html>
    <html>
        <head>
            <!-- Required meta tags -->
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <!-- Bootstrap CSS -->
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

            <link href="${pageContext.request.contextPath}/customcss.css" rel="stylesheet" > 
            <title>Servizio Sanitario</title>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

        </head>
        <body>
            <!-- Navigation -->
            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="#">Servizio Sanitario</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="home.jsp">Home
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="users.jsp">Utente</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<c:url value="../logout.handler"></c:url>">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div class="form-gap"></div>

                <div class="cards-list pb-5">

                    <div class="card 1">
                        <a href="myPatientDoctor.jsp" class="card_image"> 
                            <img src="https://source.unsplash.com/zbpgmGe27p8" /> 
                        </a>
                        <div class="card_title title-white">
                            <p>Lista Pazienti</p>
                        </div>
                    </div>

                    <div class="card 2">
                        <a href="SeePrescription.jsp" class="card_image">
                            <img src="https://source.unsplash.com/nMyM7fxpokE" />
                        </a>
                        <div class="card_title title-white">
                            <p>Esami Prescritti</p>
                        </div>
                    </div>

                    <div class="card 3">
                        <a href="PrescriptionExam.jsp" class="card_image">
                            <img src="https://source.unsplash.com/npxXWgQ33ZQ" />
                        </a>
                        <div class="card_title title-white">
                            <p>Prescrivi Esame</p>
                        </div>
                    </div>

                    <div class="card 4">
                        <a href="Prescription.jsp" class="card_image">
                            <img src="https://source.unsplash.com/OQMZwNd3ThU" />
                        </a>
                        <div class="card_title title-white">
                            <p>Prescrivi Ricetta</p>
                        </div>
                    </div>
                    <div class="card 5">
                        <a href="visits2.jsp" class="card_image">
                            <img src="https://source.unsplash.com/yo01Z-9HQAw" />
                        </a>
                        <div class="card_title title-white">
                            <p>Visite</p>
                        </div>
                    </div>
                    <div class="card 6">
                        <a href="insVisit.jsp" class="card_image">
                            <img src="https://source.unsplash.com/0LaBRkmH4fM" />
                        </a>
                        <div class="card_title title-white">
                            <p>Inserisci visita</p>
                        </div>
                    </div>

                </div>

                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
                <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>


            </body>
        </html>
</c:catch>
<c:if test="${not empty ex}">
    <jsp:scriptlet>
        response.sendError(500, ((Exception) pageContext.getAttribute("ex")).getMessage());
    </jsp:scriptlet>
</c:if>
