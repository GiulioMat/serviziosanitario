<%-- 
Document   : PrescriptionExam
Created on : 23 ago 2019, 11:15:11
Author     : Pretto Stefano
--%>
<%@page import="com.servizio.sanitario.persistence.dao.UserDAO"%>
<%@page import="com.servizio.sanitario.persistence.dao.ExamDAO"%>
<%@page import="com.servizio.sanitario.persistence.entities.Exam"%>
<%@page import="com.servizio.sanitario.persistence.dao.PrescribableDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="https://code.jquery.com/jquery-3.3.1.js">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.datepicker-fork/1.3.0/css/datepicker3.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>

        <link href="${pageContext.request.contextPath}/customcss.css" rel="stylesheet">

        <title>Prescrivi esame</title>
    </head>

    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
            <div class="container">
                <c:set var="a" value="${pageContext.request.getParameter('id_patient')}"/>
                <c:choose>
                    <c:when test="${a == null}">
                        <a class="navbar-brand" href="doctorhome.jsp">Servizio Sanitario</a>
                    </c:when>    
                    <c:otherwise>
                        <a class="navbar-brand" href="myPatientDoctor.jsp">Servizio Sanitario</a>
                    </c:otherwise>
                </c:choose>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="doctorhome.jsp">Home
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<c:url value="../logout.handler"></c:url>">Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="form-gap"></div>

        <c:set var ="name_exams" value="${prescribableDao.all}"/>
        <c:set var="length" value="${fn:length(name_exams)}"/>
        <c:set var="id" scope="session" value="${sessionScope.user.id}"/>        
        <div class="container-fluid">
            <p class="font-weight-bold">Prescrivi un esame</p>
            <form action="${pageContext.request.contextPath}/createexam.handler" method="post">
                <label class="pt-3" for="patientId">Seleziona paziente</label>  
                <c:choose>
                    <c:when test="${pageContext.request.getParameter('id_patient') == null}">
                        <select class="custom-select" name="patientId" id="patientId">
                            <c:forEach var="utente" items="${userDao.all}">
                                <c:set var="doc" value="${PatientDoctorDao.getDoctorId(utente.id)}"/>
                                <c:if test="${(utente.role == 'paziente') && (doc==id)}">
                                    <option value="${utente.id}">${utente.firstName} ${utente.surname}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </c:when>
                    <c:otherwise>
                        <c:set var="id_user" value="${pageContext.request.getParameter('id_patient')}"/>
                        <c:set var="user_request" value="${userDao.getPrimary(id_user)}"/>
                        <input type="text" name="prova" value="${user_request.firstName} ${user_request.surname}"><br>
                        <input type="hidden" name="patientId" value="${id_user}">
                    </c:otherwise>
                </c:choose>
                <label class="pt-4" for="examName">Nome esame</label>   
                <select name="examName" id="examName" class="custom-select">
                    <c:forEach var = "i" begin = "0" end = "${length}">
                        <option value="${name_exams[i].id_prescribable}">${name_exams[i].name} </option>
                    </c:forEach> 
                </select>


                <label class="pt-4" for="examName">Nome medico specialista</label>   
                <select class="custom-select" name="doctorSpecializedId" id="doctorSpecializedId">
                    <c:forEach var="doctor" items="${userDao.all}">
                        <c:if test = "${doctor.role =='MedicoSpecialista'}">
                            <option value="${doctor.id}">${doctor.firstName} ${doctor.surname} </option>
                        </c:if>
                    </c:forEach>
                </select>

                <label class="pt-4 w-100">Data</label>  
                <input class="datepicker" type="text" id="date" name="date" style="cursor: default">
                <input type="hidden" name="doctorId" value="${sessionScope.user.id}">
                <div class="w-100 pt-3"></div>
                <input type="submit" name="Button" class="mt-3 btn btn-secondary active" value="Prescrivi">
            </form> 
        </div>

        <div class="pt-5"></div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/bootstrap.datepicker-fork/1.3.0/js/bootstrap-datepicker.js"></script>
        <script src="https://cdn.jsdelivr.net/bootstrap.datepicker-fork/1.3.0/js/locales/bootstrap-datepicker.it.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $.fn.datepicker.defaults.language = 'it';
            });

            $(document).ready(function () {
                $('.datepicker').datepicker({
                    format: "yyyy-mm-dd",
                    autoclose: true
                });
                $('.datepicker').datepicker('setDate', new Date());
            });
        </script>
        <script>
            $(".custom-select").select2({
                tags: "true",
                placeholder: "Seleziona un'opzione",
                theme: "classic",
                width: '100%',
                createTag: function (params) {
                    var term = $.trim(params.term);

                    if (term === '') {
                        return null;
                    }

                    return {
                        id: term,
                        text: term,
                        value: true // add additional parameters
                    };
                }
            });
        </script>
    </body>
</html>
