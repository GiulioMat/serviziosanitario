<%@page import="java.util.List"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException"%>
<%@page import="com.servizio.sanitario.persistence.entities.User"%>
<%@page import="com.servizio.sanitario.persistence.entities.Prescription"%>
<%@page import="com.servizio.sanitario.persistence.entities.Ticket"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory"%>
<%@page import="com.servizio.sanitario.persistence.dao.UserDAO"%>
<%@page import="com.servizio.sanitario.persistence.dao.PrescriptionDAO"%>
<%@page import="com.servizio.sanitario.persistence.dao.TicketDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:catch var="ex">
    <!DOCTYPE html>
    <html>

        <head>
            <!-- Required meta tags -->
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <!-- Bootstrap CSS -->

            <link rel="stylesheet" type="text/css" href="https://code.jquery.com/jquery-3.3.1.js">
            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">
            <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

            <link href="${pageContext.request.contextPath}/customcss.css" rel="stylesheet" >

            <title>Visite</title>
        </head>

        <body>
            <c:set var="user" scope="session" value="${sessionScope.user}"/>

            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                <div class="container">
                    <c:choose>
                        <c:when test="${user.role == 'paziente'}">
                            <a class="navbar-brand" href="patienthome.jsp">Servizio Sanitario</a>
                        </c:when>    
                        <c:otherwise>
                            <a class="navbar-brand" href="doctorhome.jsp">Servizio Sanitario</a>
                        </c:otherwise>
                    </c:choose>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <c:if test="${a == null}">
                                    <a class="nav-link" href="patienthome.jsp">Home
                                        <span class="sr-only">(current)</span>
                                    </a>
                                </c:if>    
                                <c:if test="${a != null}">
                                    <a class="nav-link" href="doctorhome.jsp">Home
                                        <span class="sr-only">(current)</span>
                                    </a>
                                </c:if>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<c:url value="../logout.handler"></c:url>">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div class="form-gap"></div>
                <div class="container-fluid mb-5">
                    <table id="patientsTable" class="table table-striped table-bordered dt-responsive mb-5 nowrap" style="width: 100%">
                        <thead>
                            <tr>
                            <c:if test="${user.role == 'MedicoBase'}">
                                <th>Paziente</th>
                            </c:if>
                            <c:if test="${user.role == 'paziente'}">
                                <th>Medico</th>
                            </c:if>
                            <th>Data</th>                                  
                        </tr>
                    </thead>
                    <tbody>

                        <c:if test="${user.role == 'paziente'}">
                            <c:forEach var="visite" items="${visitDao.getAllByPatient(user.id)}">
                                <tr>    
                                    <c:set var="medico" value="${userDao.getPrimary(visite.id_doctor)}"/>
                                    <td>${medico.firstName} ${medico.surname}</td>
                                    <td>${visite.date}</td>
                                </tr>
                            </c:forEach>
                        </c:if>
                        <c:if test="${user.role == 'MedicoBase'}">
                            <c:forEach var="visite" items="${visitDao.getAllByDoctor(user.id)}">
                                <tr>    
                                    <c:set var="paziente" value="${userDao.getPrimary(visite.id_patient)}"/>
                                    <td>${paziente.firstName} ${paziente.surname}</td>
                                    <td>${visite.date}</td>
                                </tr>
                            </c:forEach>
                        </c:if>
                    </tbody>  
                    <tfoot>
                        <tr>
                        <c:if test="${user.role == 'MedicoBase'}">
                            <th>Paziente</th>
                        </c:if>
                        <c:if test="${user.role == 'paziente'}">
                            <th>Medico</th>
                        </c:if>
                        <th>Data</th>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap4.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
            <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        </body>
    </html>
    <script>

        $(document).ready(function () {
            var table = $('#patientsTable').DataTable({
                responsive: true,
                info: false
            });
        });


    </script>
</c:catch>
<c:if test="${not empty ex}">
    <jsp:scriptlet>
        response.sendError(500, ((Exception) pageContext.getAttribute("ex")).getMessage());
    </jsp:scriptlet>
</c:if>
