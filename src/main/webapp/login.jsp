<%@page import="com.servizio.sanitario.persistence.entities.User"%>
<%@page import="com.servizio.sanitario.persistence.dao.UserDAO"%>
<%@page import="javax.servlet.http.Cookie"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

        <link href="customcss.css" rel="stylesheet" type="text/css"/>

        <title>Servizio Sanitario: Autenticazione</title>
    </head>
    
    <body>
        
        <c:set var="token" value="${cookie['token'].getValue()}"/>
        <c:if test="${token!=null}">
            <jsp:forward page="login.handler"> 
                <jsp:param name="token" value="${token}" /> 
            </jsp:forward>
        </c:if>
        
        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
            <div class="container">
                <a class="navbar-brand" href="home.jsp">Servizio Sanitario</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="home.jsp">Home
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="login.jsp">Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>       
        <form class="form-signin" action="login.handler" method="POST">
            <div class="container-fluid">
                <div class="row no-gutter">
                    <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
                    <div class="col-md-8 col-lg-6">
                        <div class="login d-flex align-items-center py-5">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-9 col-lg-8 mx-auto">
                                        <h3 class="login-heading mb-4">Benvenuto!</h3>
                                        <form>
                                            <div class="form-label-group">
                                                <input type="email" id="username" name="username" class="form-control" placeholder="Email address" value="" required autofocus>
                                                <label for="username">Indirizzo e-mail</label>
                                            </div>

                                            <div class="form-label-group">
                                                <input type="password" id="password" name="password" class="form-control" placeholder="Password" value="" required>
                                                <label for="password">Password</label>
                                            </div>

                                            <div class="custom-control custom-checkbox mb-3">
                                                <input type="checkbox" class="custom-control-input" checked="true" id="rememberMe" name="rememberMe" value="1">
                                                <label class="custom-control-label" for="rememberMe">Mantieni l'accesso</label>
                                            </div>
                                            <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Accedi</button>
                                            <div class="text-center">
                                                <a class="small" href="forgotPassword.html">Hai dimenticato la password?</a></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </form>
        
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    </body>

</html>
