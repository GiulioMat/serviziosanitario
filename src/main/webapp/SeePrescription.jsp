<%-- 
    Document   : SeePrescription
    Created on : 25 ago 2019, 12:01:13
    Author     : Pretto Stefano
--%>

<%@page import="java.util.List"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException"%>
<%@page import="com.servizio.sanitario.persistence.entities.Exam"%>
<%@page import="com.servizio.sanitario.persistence.entities.User"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory"%>
<%@page import="com.servizio.sanitario.persistence.dao.ExamDAO"%>
<%@page import="com.servizio.sanitario.persistence.dao.UserDAO"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->

        <link rel="stylesheet" type="text/css" href="https://code.jquery.com/jquery-3.3.1.js">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

        <link href="${pageContext.request.contextPath}/customcss.css" rel="stylesheet" >

        <title>Esami Prescritti</title>
    </head>

    <body>

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
            <div class="container">
                <c:set var="url" value="${pageContext.request.getParameter('id_patient')}" />
                <c:choose>
                    <c:when test="${url == null}">
                        <a class="navbar-brand" href="doctorhome.jsp">Servizio Sanitario</a>
                    </c:when>    
                    <c:otherwise>
                        <a class="navbar-brand" href="myPatientDoctor.jsp">Servizio Sanitario</a>
                    </c:otherwise>
                </c:choose>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="doctorhome.jsp">Home
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<c:url value="../logout.handler"></c:url>">Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="form-gap"></div>

            <div class="container-fluid">    
                <!-- Table -->
                <table id="seePrescriptionTable" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%">
                    <thead>
                        <tr>    
                            <th>Nome Paziente</th>
                            <th>Descrizione</th>
                            <th>Data</th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:set var="id_user" value="${sessionScope.user.id}"/>
                    <c:choose>
                        <c:when test="${url == null}">
                            <c:set var="id_user" scope="session" value="${sessionScope.user.id}"/>
                        </c:when>    
                        <c:otherwise>
                            <c:set var="id_user" value="${pageContext.request.getParameter('id_patient')}"/>
                        </c:otherwise>
                    </c:choose>
                    <c:forEach var="exam" items="${examDao.all}">    
                        <c:if test="${exam.id_doctor == id_user}">
                            <tr> 
                                <td>
                                    <c:set var="paziente" value="${userDao.getPrimary(exam.id_patient)}"/>  
                                    ${paziente.firstName} ${paziente.surname}
                                </td> 
                                <c:set var="prescribable" value="${prescribableDao.getByPrimaryKey(exam.id_prescribable)}"/>
                                <td>
                                    ${prescribable.name}
                                </td>
                                <td>
                                    ${exam.date_exam}
                                </td>
                            </tr> 
                        </c:if>
                    </c:forEach>                             
                </tbody>  
                <tfoot>
                    <tr>
                        <th>Nome Paziente</th>
                        <th>Descrizione</th>
                        <th>Data</th>
                    </tr>
                </tfoot>
            </table>
        </div>

        <div class=" mb-5"></div>

        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap4.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                var table = $('#seePrescriptionTable').DataTable({
                    responsive: true,
                    info: false
                });
            });
        </script>
    </body>

</html>
