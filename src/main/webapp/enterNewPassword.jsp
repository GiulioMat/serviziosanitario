<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@page import="java.sql.*"%>
<%@page import="java.io.*"%>
                                                
<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

        <link href="customcss.css" rel="stylesheet" type="text/css"/>

        <title>Servizio Sanitario: Nuova Password</title>
    </head>
    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
            <div class="container">
                <a class="navbar-brand" href="home.jsp">Servizio Sanitario</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="home.jsp">Home
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="login.jsp">Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="form-gap"></div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="text-center">
                                <h3><i class="fa fa-unlock fa-4x"></i></h3>
                                <h2 class="text-center">Nuova password</h2>
                                <p>Inserisci una nuova password</p>
                                <div class="panel-body">

                                    <form class="form-signin" method="GET" name = "f_psw" action="changePassword.handler">
                                        <div class="form-group">
                                            
                                            <div class="form-label-group">
                                                <input type="password" id="password1" name="password1" class="form-control" placeholder="Nuova Password" required autofocus>
                                                <label for="password1">Nuova Password</label>
                                                <input class="mt-2" type="hidden" id="email" name="email" value="${sessionScope.user.email}">
                                            </div>

                                            <div class="form-label-group">
                                                <input type="password" id="password2" name="password2" class="form-control" placeholder="Conferma Password" required autofocus>
                                                <label for="password2">Conferma Password</label>
                                            </div>
                                            <input class="mt-2" type="hidden" id="email" name="email" value="${sessionScope.user.email}">
                                        </div>
                                        <div class="form-group">
                                            <input class="btn btn-lg btn-primary btn-block" value="Conferma" type="button" onclick="controllo()">
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" language="JavaScript">
            function controllo(){
                if(document.getElementById("password1").value != document.getElementById("password2").value) {
                    alert("Le due password non corrispondono");
                    return false;
                } else {
                    document.f_psw.submit();
                }
            }
        </script>
    </body>
</html>
