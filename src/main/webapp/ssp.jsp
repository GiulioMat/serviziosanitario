<%-- 
    Document   : ssp
    Created on : 3 ott 2019, 10:46:22
    Author     : asus
--%>



<%@page import="java.util.List"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException"%>
<%@page import="com.servizio.sanitario.persistence.entities.User"%>
<%@page import="com.servizio.sanitario.persistence.entities.Prescription"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory"%>
<%@page import="com.servizio.sanitario.persistence.dao.UserDAO"%>
<%@page import="com.servizio.sanitario.persistence.dao.PrescriptionDAO"%>
<%@page import="java.net.URLEncoder"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:catch var="ex">
    <!DOCTYPE html>
    <html>
        <head>
            <!-- Required meta tags -->
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

            <!-- Bootstrap CSS -->
            <link rel="stylesheet" type="text/css" href="https://code.jquery.com/jquery-3.3.1.js">
            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap4.min.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.1/css/select.bootstrap4.min.css">
            <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

            <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.datepicker-fork/1.3.0/css/datepicker3.css">

            <link href="${pageContext.request.contextPath}/customcss.css" rel="stylesheet">

            <title>Servizio Sanitario Provinciale</title>
        </head>

        <body>
            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="users.jsp">Servizio Sanitario</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="users.jsp">Home
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<c:url value="../logout.handler"></c:url>">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div class="form-gap"></div>

                <div class="container-fluid mb-5">
                    <div class="container-fluid text-center mt-3">
                        <input class="datepicker" type="text" id="date" name="date" style="cursor: default">
                    </div>
                    <div class="mt-5 w-100">
                        <!--Table-->
                        <table id="sspTable" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Farmaco</th>
                                    <th>Paziente</th>
                                    <th>Medico di base</th>
                                    <th>Farmacia</th>
                                    <th>Valore Ticket</th>
                                    <th>Data Erogazione</th>
                                </tr>
                            </thead>

                        <c:set var="user" scope="session" value="${sessionScope.user}"/>
                        <c:forEach var="prescription" items="${prescriptionDao.all}">   
                            <c:forEach var="pharmacy" items="${pharmacyDao.getByProvince(user.province)}">
                                <c:if test="${prescription.id_pharmacy == pharmacy.id}">
                                    <tr>
                                        <td>${prescription.description}</td>
                                        <c:set var="patient" value="${userDao.getPrimary(prescription.id_patient)}"/>
                                        <td>${patient.firstName} ${patient.surname}</td>
                                        <c:set var="doctor" value="${userDao.getPrimary(prescription.id_doctor)}"/>
                                        <td>${doctor.firstName} ${doctor.surname}</td>
                                        <td>${pharmacy.name}</td>
                                        <c:set var="ticket" value="${ticketDao.getByPrimaryKey(prescription.id_ticket)}"/>
                                        <td>${ticket.value}</td>
                                        <td>${prescription.date_erog}</td>
                                    </tr> 
                                </c:if>
                            </c:forEach>
                        </c:forEach>
                        <tfoot>
                            <tr>
                                <th>Farmaco</th>
                                <th>Paziente</th>
                                <th>Medico di base</th>
                                <th>Farmacia</th>
                                <th>Valore Ticket</th>
                                <th>Data Erogazione</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
            <script src="https://cdn.jsdelivr.net/bootstrap.datepicker-fork/1.3.0/js/bootstrap-datepicker.js"></script>
            <script src="https://cdn.jsdelivr.net/bootstrap.datepicker-fork/1.3.0/js/locales/bootstrap-datepicker.it.js"></script>

            
            <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap4.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

            <script type="text/javascript">
                $(document).ready(function () {
                    $.fn.datepicker.defaults.language = 'it';
                });

                $(document).ready(function () {
                    $('.datepicker').datepicker({
                        format: "yyyy-mm-dd",
                        autoclose: true
                    });
                    $('.datepicker').datepicker('setDate', new Date());
                });


                $.fn.dataTable.ext.search.push(
                        function (settings, data, dataIndex) {
                            var dateSelected = $('#date').val();
                            var date = data[5];
                            return dateSelected === "" || date === dateSelected;
                        }
                );

                $(document).ready(function () {
                    var table = $('#sspTable').DataTable({
                        dom: "<'row'<'col-sm-12 col-md-4'l><'col-sm-12 col-md-4 text-center'B><'col-sm-12 col-md-4'f>>" + "trp",
                        buttons: [{
                                extend: 'excel',
                                text: 'Excel'
                            }],
                        responsive: true
                    });

                    $('#date').on('keyup change', function () {
                        table.draw();
                    });
                });
            </script>


        </body>
    </html>

</c:catch>
<c:if test="${not empty ex}">
    <jsp:scriptlet>
        response.sendError(500, ((Exception) pageContext.getAttribute("ex")).getMessage());
    </jsp:scriptlet>
</c:if>