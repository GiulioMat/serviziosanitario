<%@page import="com.servizio.sanitario.persistence.dao.UserDAO"%>
<%@page import="com.servizio.sanitario.persistence.dao.ExamDAO"%>
<%@page import="com.servizio.sanitario.persistence.dao.PatientDoctorDAO"%>
<%@page import="com.servizio.sanitario.persistence.entities.Exam"%>
<%@page import="com.servizio.sanitario.persistence.entities.Patient_Doctor"%>
<%@page import="com.servizio.sanitario.persistence.dao.PrescribableDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<c:catch var="ex">
<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="https://code.jquery.com/jquery-3.3.1.js">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.datepicker-fork/1.3.0/css/datepicker3.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>

        <link href="${pageContext.request.contextPath}/customcss.css" rel="stylesheet">

        <title>Inserisci visita</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="doctorhome.jsp">Servizio Sanitario</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="doctorhome.jsp">Home
                                    <span class="sr-only">(current)</span>
                                </a> 
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<c:url value="../logout.handler"></c:url>">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

        <div class="form-gap"></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="text-center">
                                <h3><i class="fa fa-lock fa-4x"></i></h3>
                                <h2 class="text-center">Scegli il paziente</h2>
                                <div class="panel-body">
                                    <form class="form-signin" action="insertVisit.handler" method="POST">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                                                <select class="custom-select" name="patientId" id="patientId" required="true">
                                                    <c:set var="id" scope="session" value="${sessionScope.user.id}"/>
                                                    <c:forEach var="patient" items="${userDao.getPatientDoctor(id)}">
                                                        <c:if test = "${patient.role =='paziente'}">
                                                            <option value="${patient.id}">${patient.firstName} ${patient.surname}</option>
                                                        </c:if>
                                                    </c:forEach> 
                                                </select>
                                                <input type="hidden" name="doctorId" value="${sessionScope.user.id}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" name="Button" class="mt-3 btn btn-secondary active" value="Inserisci">
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    </body>

</html>
</c:catch>
<c:if test="${not empty ex}">
    <jsp:scriptlet>
        response.sendError(500, ((Exception) pageContext.getAttribute("ex")).getMessage());
    </jsp:scriptlet>
</c:if>